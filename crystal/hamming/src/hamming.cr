module Hamming
  def self.compute(a, b)
    if a.size != b.size
      raise ArgumentError.new("Inputs must be of equal length")
    end

    (0...a.size).count do |x|
      a[x] != b[x]
    end
  end
end
