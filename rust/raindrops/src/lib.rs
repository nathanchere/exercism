pub fn raindrops(n: u32) -> String {
    let mut result = String::new();

    if is_factor(n, 3) { result.push_str("Pling"); }
    if is_factor(n, 5) { result.push_str("Plang"); }
    if is_factor(n, 7) { result.push_str("Plong"); }

    if result.is_empty() { result = n.to_string(); }
    return result;
}

pub fn is_factor(number: u32, factor: u32) -> bool
{
    return number % factor == 0;
}