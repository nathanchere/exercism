pub fn find() -> Option<u32>
{
    let target: u32 = 1000;
    for a in 3..(target / 3)
    {
        for b in (a + 1)..(target / 2) 
        {
            let c = target - a - b;
            if is_pythagorean_triplet(a,b,c) {
                return Some(a * b * c);
            }
        }
    }
    return None;
}

pub fn is_pythagorean_triplet(a: u32, b: u32, c: u32) -> bool
{
    return a.pow(2) + b.pow(2) == c.pow(2);
}