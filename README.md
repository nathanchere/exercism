# Exercism solutions

## Exercism CLI

[Installation instructions](https://exercism.io/cli)

## To run

- C#
  - [Exercism C# track](https://exercism.io/my/tracks/csharp)
  - [.Net Core documentation](https://docs.microsoft.com/en-us/dotnet/core/)
  - [C# language documentation](https://docs.microsoft.com/en-us/dotnet/csharp/)
  - Run: `dotnet test`

- Crystal
  - [Exercism Crystal track](https://exercism.io/my/tracks/crystal)
  - [Installing Crystal](http://crystal-lang.org/docs/installation/index.html)
  - [Crystal language documentation](https://crystal-lang.org/reference/)
  - Run: `crystal spec`

- Elixir
  - [Exercism Elixir track](https://exercism.io/my/tracks/elixir)
  - [Elixir language documenation](https://elixir-lang.org/docs.html)
  - Run: `elixir {filename}` (e.g. `elixir hello_world.exs`)

- Elm
  - [Exercism Elm track](https://exercism.io/my/tracks/elm)
  - Setup (per challenge): `elm-package install --yes`
  - Run: `elm-test`

- JavaScript
  - [Exercism JavaScript track](https://exercism.io/my/tracks/javascript)
  - [JavaScript language documentation](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference)
  - Setup (per challenge): `npm install`
  - Run: `npm test`

- Python
  - [Exercism Python track](https://exercism.io/my/tracks/python)
  - [Python language documentation](https://docs.python.org)
  - Setup (once): `sudo pip install pytest`
  - Run: `pytest`

- Rust
  - [Exercism Rust track](https://exercism.io/my/tracks/rust)
  - [Rust language documentation](https://doc.rust-lang.org)
  - Run: `cargo test`
