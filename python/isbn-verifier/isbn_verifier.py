import re


def is_valid(isbn):
    if not re.match(r"^\d\-{0,1}\d{3}\-{0,1}\d{5}\-{0,1}[\dX]$", isbn):
        return False

    digits = [parse(digit) for digit in re.sub(r'[^0-9X]+', '', isbn)]
    result = sum([digits[i] * (10-i) for i in range(0, 10)])
    return result % 11 == 0


def parse(digit):
    if(digit == 'X'):
        return 10
    return int(digit)
