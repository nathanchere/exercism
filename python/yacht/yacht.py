# Score categories
YACHT = 1
ONES = 2
TWOS = 3
THREES = 4
FOURS = 5
FIVES = 6
SIXES = 7
FULL_HOUSE = 8
FOUR_OF_A_KIND = 9
LITTLE_STRAIGHT = 10
BIG_STRAIGHT = 11
CHOICE = 12


def score(dice, category):
    scorer = {
        YACHT: Yacht,
        ONES: Ones,
        TWOS: Twos,
        THREES: Threes,
        FOURS: Fours,
        FIVES: Fives,
        SIXES: Sixes,
        FULL_HOUSE: FullHouse,
        FOUR_OF_A_KIND: FourOfAKind,
        LITTLE_STRAIGHT: LittleStraight,
        BIG_STRAIGHT: BigStraight,
        CHOICE: Choice
    }.get(category)
    return scorer(dice)


def Yacht(dice):
    if group(dice)[0][1] == 5:
        return 50
    return 0


def Choice(dice):
    return sum(dice)


def BigStraight(dice):
    if is_straight(dice, 2):
        return 30
    return 0


def LittleStraight(dice):
    if is_straight(dice, 1):
        return 30
    return 0


def FourOfAKind(dice):
    grouped_dice = group(dice)
    if grouped_dice[0][1] >= 4:
        return grouped_dice[0][0] * 4
    return 0


def FullHouse(dice):
    grouped_dice = group(dice)
    if grouped_dice[0][1] == 3 and grouped_dice[1][1] == 2:
        return sum(dice)
    return 0


def Sixes(dice):
    return count(dice, 6)


def Fives(dice):
    return count(dice, 5)


def Fours(dice):
    return count(dice, 4)


def Threes(dice):
    return count(dice, 3)


def Twos(dice):
    return count(dice, 2)


def Ones(dice):
    return count(dice, 1)


def count(dice, target):
    return sum([die for die in dice if die == target])


def is_straight(dice, offset):
    sorted_dice = sorted(dice)
    for i in range(0, 5):
        if sorted_dice[i] != offset + i:
            return False
    return True


def group(dice):
    result = {}
    for die in dice:
        result[die] = result.get(die, 0) + 1
    return sorted(result.items(), key=lambda item: item[1], reverse=True)
