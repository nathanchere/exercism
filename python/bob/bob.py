def response(hey_bob):
    upper = str.upper(hey_bob)
    is_shouting = upper == hey_bob and upper != str.lower(hey_bob)
    is_asking = str.rstrip(hey_bob).endswith("?")
    is_silence = not str.strip(hey_bob)

    if is_shouting and is_asking:
        return "Calm down, I know what I'm doing!"
    if is_shouting:
        return "Whoa, chill out!"
    if is_asking:
        return "Sure."
    if is_silence:
        return "Fine. Be that way!"
    return "Whatever."
