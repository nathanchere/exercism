import re


class Phone(object):
    def __init__(self, phone_number):
        cleaned = re.sub(r"[^0-9]", "", phone_number)
        if len(cleaned) == 11 and cleaned[0:1] == '1':
            cleaned = cleaned[1:]
        if re.match(r"^[2-9]\d{2}[2-9]\d{6}$", cleaned) == None:
            raise ValueError("Invalid format")
        self.number = cleaned
