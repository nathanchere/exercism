import random


class Character:
    def __init__(self):
        self.strength = self.ability()
        self.dexterity = self.ability()
        self.constitution = self.ability()
        self.intelligence = self.ability()
        self.wisdom = self.ability()
        self.charisma = self.ability()
        self.hitpoints = 10 + modifier(self.constitution)

    def ability(self):
        dice = [roll_die(), roll_die(), roll_die(), roll_die()]
        best_dice = sorted(dice)[1:]
        return sum(best_dice)


def modifier(score):
    return round((score - 10.5) / 2)


def roll_die():
    return random.randint(1, 6)
