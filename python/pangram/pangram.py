def is_pangram(sentence):
    alphabet = set('abcdefghijklmnopqrstuvwxyz')
    target = set(str.lower(sentence))
    return len(alphabet - target) == 0
