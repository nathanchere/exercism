import re


def is_isogram(string):
    clean = re.sub(r"[^a-z]", "", str.lower(string))
    return len(clean) == len(set(clean))
