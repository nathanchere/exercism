def steps(number):

    if number <= 0:
        raise ValueError("Must enter a positive integer")

    result = 0

    while number != 1:
        if number % 2 == 0:
            number = number / 2
        else:
            number = number * 3 + 1
        result = result + 1

    return result
