def proteins(strand):
    protein_map = get_protein_map()
    result = []
    while(strand):
        protein = protein_map[strand[:3]]
        if(protein == 'STOP'):
            break
        result = result + [protein]
        strand = strand[3:]
    return result


def get_protein_map():
    return dict([
        ('AUG', 'Methionine'),
        ('UUU', 'Phenylalanine'),
        ('UUC', 'Phenylalanine'),
        ('UUA', 'Leucine'),
        ('UUG', 'Leucine'),
        ('UCU', 'Serine'),
        ('UCC', 'Serine'),
        ('UCA', 'Serine'),
        ('UCG', 'Serine'),
        ('UAC', 'Tyrosine'),
        ('UAU', 'Tyrosine'),
        ('UGC', 'Cysteine'),
        ('UGU', 'Cysteine'),
        ('UGG', 'Tryptophan'),
        ('UGA', 'STOP'),
        ('UAG', 'STOP'),
        ('UAA', 'STOP')
    ])
