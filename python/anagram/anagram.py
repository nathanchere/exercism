def find_anagrams(word, candidates):
    sorted_word = sort(word)
    return [x for x in candidates
            if (str.lower(x) != str.lower(word))
            and sort(x) == sorted_word]


def sort(word):
    return ''.join(sorted(str.lower(word)))
