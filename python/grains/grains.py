def square(number):
    if number < 1 or number > 64:
        raise ValueError("Invalid square")
    return pow(2, number - 1)


def total(number):
    if number < 1 or number > 64:
        raise ValueError("Invalid square")
    return pow(2, number) - 1
