def equilateral(sides):
    return is_valid(sides) and sides[0] == sides[1] and sides[1] == sides[2]


def isosceles(sides):
    return is_valid(sides) and ((sides[0] == sides[1]) ^ (sides[1] == sides[2]) ^ (sides[0] == sides[2]))


def scalene(sides):
    return is_valid(sides) and sides[0] != sides[1] and sides[1] != sides[2]


def is_valid(sides):
    if sides[0] <= 0 or sides[1] <= 0 or sides[2] <= 0:
        return False
    if sides[0] + sides[1] <= sides[2]:
        return False
    if sides[0] + sides[2] <= sides[1]:
        return False
    if sides[1] + sides[2] <= sides[0]:
        return False
    return True
