ADENINE = 'A'
CYTOSINE = 'C'
GUANINE = 'G'
THYMINE = 'T'
URACIL = 'U'


def to_rna(dna_strand):
    return ''.join([transcribe(i) for i in dna_strand])


def transcribe(dna):
    if dna == CYTOSINE:
        return GUANINE
    if dna == GUANINE:
        return CYTOSINE
    if dna == THYMINE:
        return ADENINE
    if dna == ADENINE:
        return URACIL
    raise Exception("Invalid input")
