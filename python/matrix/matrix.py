class Matrix(object):
    def __init__(self, matrix_string):
        self.rows = [
            [int(i) for i in row.split()]
            for row in matrix_string.splitlines()
        ]

        self.columns = [list(col) for col in zip(*self.rows)]

    def row(self, index):
        return list(self.rows[index-1])

    def column(self, index):
        return list(self.columns[index-1])
