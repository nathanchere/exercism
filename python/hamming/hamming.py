from functools import reduce


def distance(strand_a, strand_b):
    if len(strand_a) != len(strand_b):
        raise ValueError("Inputs must be equal length")

    return len([
        None for i in zip(strand_a, strand_b)
        if i[0] != i[1]
    ])
