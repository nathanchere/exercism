﻿using System;
using System.Linq;

public static class Pangram
{
    public static bool IsPangram(string input) =>
        input
        .ToLowerInvariant()
        .ToCharArray()
        .Distinct()
        .Where(char.IsLetter)
        .Count() == 26;
}
