using System;
using System.Collections.Generic;
using System.Linq;

public class GradeSchool
{
    private readonly Dictionary<int, List<string>> _roster = new Dictionary<int, List<string>>();

    public GradeSchool(int maxGrade = 12)
    {
        for (int i = 1; i <= maxGrade; i++)
        {
            _roster.Add(i, new List<string>());
        }
    }

    public void Add(string student, int grade)
    {
        _roster[grade].Add(student);
        _roster[grade].Sort();
    }

    public IEnumerable<string> Roster() =>
        _roster.Values.SelectMany(student => student);


    public IEnumerable<string> Grade(int grade) =>
         _roster[grade];
}