﻿using System;
using System.Linq;
using System.Collections.Generic;

public enum Classification
{
    Perfect,
    Abundant,
    Deficient
}

public static class PerfectNumbers
{
    public static Classification Classify(int number)
    {
        if (number < 1) throw new ArgumentOutOfRangeException();
        var aliquotSum = GetFactors(number).Sum();

        if (aliquotSum < number) return Classification.Deficient;
        if (aliquotSum > number) return Classification.Abundant;
        return Classification.Perfect;
    }

    private static IEnumerable<int> GetFactors(int number)
    {
        for (int x = (int)(number / 2); x >= 1; x--)
        {
            if (number % x == 0) yield return x;
        }
    }
}
