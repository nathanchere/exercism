﻿using System;
using System.Collections.Generic;

public static class SecretHandshake
{
    [Flags]
    private enum Command
    {
        None = 0,
        Wink = 1,
        DoubleBlink = 2,
        CloseYourEyes = 4,
        Jump = 8,
        Reverse = 16
    }

    public static string[] Commands(int commandValue)
    {
        var result = new List<string>();
        var commands = (Command)commandValue;

        if (commands.HasFlag(Command.Wink)) result.Add("wink");
        if (commands.HasFlag(Command.DoubleBlink)) result.Add("double blink");
        if (commands.HasFlag(Command.CloseYourEyes)) result.Add("close your eyes");
        if (commands.HasFlag(Command.Jump)) result.Add("jump");
        if (commands.HasFlag(Command.Reverse)) result.Reverse();

        return result.ToArray();
    }
}
