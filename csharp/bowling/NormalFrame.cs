using System;
using System.Linq;

public class NormalFrame : Frame
{
    public NormalFrame(Frame previousFrame = null) : base(previousFrame)
    {
    }

    public override int Score()
    {
        if (IsStrike() || IsSpare())
        {
            return GetProjectedRolls().Take(3).Sum();
        }
        return _rolls.Sum();
    }

    protected override void ValidateRoll(int pins)
    {
        if (pins < 0)
        {
            throw new ArgumentException(ErrorNegativePins);
        }
        if (_rolls.Sum() + pins > 10)
        {
            throw new ArgumentException(ErrorTooManyPins);
        }
    }

    public override bool IsComplete() => _rolls.Count == 2 || IsStrike();
    public override bool IsStrike() => _rolls.FirstOrDefault() == 10;
    public override bool IsSpare() => !IsStrike() && _rolls.Take(2).Sum() == 10;
}
