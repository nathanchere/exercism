using System.Collections.Generic;

public abstract class Frame
{
    protected static readonly string ErrorNegativePins = "Cannot knock down negative numbers of pins";
    protected static readonly string ErrorTooManyPins = "Cannot knock down more pins than the total that are still standing";

    public int Number { get; }

    protected Frame _nextFrame { get; set; }

    protected readonly List<int> _rolls;

    protected Frame(Frame previousFrame = null)
    {
        Number = (previousFrame?.Number ?? 0) + 1;
        if (previousFrame != null) previousFrame._nextFrame = this;
        _rolls = new List<int>();
    }

    public void AddRoll(int pins)
    {
        ValidateRoll(pins);
        _rolls.Add(pins);
    }

    public abstract int Score();
    public abstract bool IsComplete();
    public abstract bool IsStrike();
    public abstract bool IsSpare();

    protected abstract void ValidateRoll(int pins);

    // Iterate over all rolls from this frame onwards
    protected IEnumerable<int> GetProjectedRolls()
    {
        var nextFrame = this;

        while (nextFrame != null)
        {
            foreach (var roll in nextFrame._rolls)
            {
                yield return roll;
            }
            nextFrame = nextFrame._nextFrame;
        }
    }

    public override string ToString()
    {
        var rolls = string.Join(",", _rolls);
        var specialStatus =
            IsStrike() ? " Strike!"
            : IsSpare() ? " Spare!"
            : "";
        return $"Frame #{Number} Rolls: [{rolls}{specialStatus}] Score: {Score()}";
    }
}