using System;
using System.Linq;

public class TenthFrame : Frame
{
    public TenthFrame(Frame previousFrame = null) : base(previousFrame)
    {
    }

    public override bool IsStrike() => false;
    public override bool IsSpare() => false;
    public override bool IsComplete()
    {
        if (_rolls.FirstOrDefault() == 10)
        {
            return _rolls.Count == 3;
        }
        if (_rolls.Take(2).Sum() == 10)
        {
            return _rolls.Count == 3;
        }
        return _rolls.Count == 2;
    }

    public override int Score() => _rolls.Sum();

    protected override void ValidateRoll(int pins)
    {
        if (pins < 0)
        {
            throw new ArgumentException(ErrorNegativePins);
        }

        if (pins > 10)
        {
            throw new ArgumentException(ErrorTooManyPins);
        }

        if (_rolls.Count < 2) return;

        if (_rolls.FirstOrDefault() != 10)
        {
            if (_rolls.Take(2).Sum() > 10)
            {
                throw new ArgumentException(ErrorTooManyPins);
            }
        }
        else
        {
            if (_rolls[1] != 10 && _rolls[1] + pins > 10)
            {
                throw new ArgumentException(ErrorTooManyPins);
            }
        }
    }
}
