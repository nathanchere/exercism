﻿using System;
using System.Collections.Generic;
using System.Linq;

public class BowlingGame
{
    private readonly List<Frame> _frames;
    private Frame _currentFrame;

    public BowlingGame()
    {
        _frames = new List<Frame>();
        AddFrame();
    }

    public void Roll(int pins)
    {
        if (_currentFrame.IsComplete())
        {
            AddFrame();
        }

        _currentFrame.AddRoll(pins);
    }

    public int? Score()
    {
        if (_frames.Count != 10 || !_currentFrame.IsComplete())
        {
            throw new ArgumentException("Cannot score an incomplete game");
        }
        return _frames.Sum(f => f.Score());
    }

    private void AddFrame()
    {
        var frameCount = _frames.Count;
        if (frameCount >= 10)
        {
            throw new ArgumentException("Cannot add more frames after a game is completed");
        }

        Frame newFrame = null;
        switch (frameCount)
        {
            case 9:
                newFrame = new TenthFrame(_currentFrame);
                break;
            default:
                newFrame = new NormalFrame(_currentFrame);
                break;
        }
        _frames.Add(newFrame);
        _currentFrame = newFrame;
    }
}
