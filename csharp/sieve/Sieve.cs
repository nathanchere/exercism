using System;
using System.Collections.Generic;
using System.Linq;

public static class Sieve
{
    public static int[] Primes(int maxValue) => FindPrimes(maxValue).ToArray();

    private static IEnumerable<int> FindPrimes(int maxValue)
    {
        if (maxValue < 2)
        {
            throw new ArgumentOutOfRangeException("No prime numbers less than 2 exist");
        }
        yield return 2;

        if (maxValue == 2)
        {
            yield break;
        }

        var flagCount = (int)Math.Floor(maxValue * 0.5);
        var flags = new bool[flagCount];

        for (int i = 3; i <= maxValue; i += 2)
        {
            var iX = ToIndexOffset(i);
            if (!flags[iX])
            {
                yield return i;

                // We increment by 2*i here because we don't care about even numbers
                // and we use long for j as a lazy way of avoiding wrapping to negative numbers
                for (long j = i; j <= maxValue; j += i + i)
                {
                    flags[ToIndexOffset((int)j)] = true;
                }
            }
        }
    }

    /// <summary>
    /// Map a series of 3-based odd numbers to a series of 0-based real numbers
    /// </summary>
    /// <remarks>Assumes input will only ever be an odd number >= 3</remarks>
    /// <example>ToIndexOffset(3) == 0</example>
    /// <example>ToIndexOffset(5) == 1</example>
    /// <example>ToIndexOffset(7) == 2</example>
    /// <example>ToIndexOffset(9) == 3</example>
    private static int ToIndexOffset(int index)
    {
        if (index < 3 || index % 2 == 0)
        {
            throw new ArgumentException("Can only index odd numbers greater than 2");
        }

        return (int)((index - 3) * 0.5);
    }
}