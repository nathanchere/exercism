using System;
using System.Collections.Generic;
using System.Linq;

public static class Series
{
    public static string[] Slices(string numbers, int sliceLength)
    {
        if (sliceLength < 1) throw new ArgumentException();
        if (sliceLength > numbers.Length) throw new ArgumentException();
        if (String.IsNullOrEmpty(numbers)) throw new ArgumentException();
        if (numbers.Any(c => !char.IsNumber(c))) throw new ArgumentException();

        return FindSlices(numbers, sliceLength).ToArray();
    }

    private static IEnumerable<string> FindSlices(string numbers, int sliceLength)
    {
        for (int i = 0; i <= numbers.Length - sliceLength; i++)
        {
            yield return numbers.Substring(i, sliceLength);
        }
    }
}