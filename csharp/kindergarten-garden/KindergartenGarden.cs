﻿using System;
using System.Collections.Generic;
using System.Linq;

public enum Plant
{
    None,
    Violets,
    Radishes,
    Clover,
    Grass
}

public class KindergartenGarden
{
    private readonly string[] _students;
    private readonly Plant[][] _plants;
    private const int BoxWidth = 2;

    public KindergartenGarden(string diagram)
    {
        _students = LoadStudents();
        _plants = diagram
            .Split('\n')
            .Select(row =>
                row.Select(c =>
                    MapTokenToPlant(c)
                ).ToArray()
            ).ToArray();
    }

    public IEnumerable<Plant> Plants(string student)
    {
        var studentIndex = Array.IndexOf(_students, student) * BoxWidth;
        foreach (var row in _plants)
        {
            foreach (var plant in row.Skip(studentIndex).Take(BoxWidth))
            {
                yield return plant;
            }
        }
    }

    private Plant MapTokenToPlant(char token)
    {
        switch (token)
        {
            case 'V': return Plant.Violets;
            case 'C': return Plant.Clover;
            case 'R': return Plant.Radishes;
            case 'G': return Plant.Grass;
            default: throw new ArgumentException();
        }
    }

    private string[] LoadStudents() => new[] {
        "Alice",
        "Bob",
        "Charlie",
        "David",
        "Eve",
        "Fred",
        "Ginny",
        "Harriet",
        "Ileana",
        "Joseph",
        "Kincaid",
        "Larry"
    };
}