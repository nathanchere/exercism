using System;

public class Clock
{
    public Clock(int hours, int minutes)
    {
        var (normalisedMinutes, hoursOver) = Util.Wrap(minutes, 60);
        Hours = Util.Wrap(hours + hoursOver, 24).Value;
        Minutes = normalisedMinutes;
    }

    public int Hours { get; }
    public int Minutes { get; }

    public Clock Add(int minutesToAdd) => new Clock(Hours, Minutes + minutesToAdd);

    public Clock Subtract(int minutesToSubtract) => Add(-minutesToSubtract);

    public override string ToString() => $"{Hours:D2}:{Minutes:D2}";

    public override bool Equals(object obj) =>
        (obj as Clock)?.ToString() == this.ToString();
}
