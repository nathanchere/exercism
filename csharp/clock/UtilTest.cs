// This file was auto-generated based on version 2.4.0 of the canonical data.

using Xunit;

public static class UtilTest
{
    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    [InlineData(59)]
    public static void Wrap_returns_correct_minutes_without_wrapping(int input)
    {
        var expected = input;

        var (value, _) = Util.Wrap(input, 60);

        Assert.Equal(expected, value);
    }

    [Theory]
    [InlineData(60, 0)]
    [InlineData(61, 1)]
    [InlineData(133, 13)]
    public static void Wrap_returns_correct_minutes_with_positive_wrapping(int input, int expected)
    {
        var (value, _) = Util.Wrap(input, 60);

        Assert.Equal(expected, value);
    }

    [Theory]
    [InlineData(-60, 0)]
    [InlineData(-1, 59)]
    [InlineData(-123, 57)]
    public static void Wrap_returns_correct_minutes_with_negative_wrapping(int input, int expected)
    {
        var (value, _) = Util.Wrap(input, 60);

        Assert.Equal(expected, value);
    }

    [Theory]
    [InlineData(59, 0)]
    [InlineData(60, 1)]
    [InlineData(61, 1)]
    [InlineData(133, 2)]
    public static void Wrap_returns_correct_hours_over_with_positive_wrapping(int input, int expected)
    {
        var (_, over) = Util.Wrap(input, 60);

        Assert.Equal(expected, over);
    }

    [Theory]
    [InlineData(-1, -1)]
    [InlineData(-59, -1)]
    [InlineData(-60, -1)]
    [InlineData(-123, -3)]
    public static void Wrap_returns_correct_hours_over_with_negative_wrapping(int input, int expected)
    {
        var (_, over) = Util.Wrap(input, 60);

        Assert.Equal(expected, over);
    }

}