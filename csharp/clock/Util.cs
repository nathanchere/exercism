public static class Util
{
    public static (int Value, int Over) Wrap(int value, int max)
    {
        var result = value;
        var over = 0;
        while (result < 0)
        {
            result += max;
            over--;
        }
        while (result >= max)
        {
            result -= max;
            over++;
        }
        return (result, over);
    }
}