using System;
using System.Text.RegularExpressions;

public static class PhoneNumber
{
    public static string Clean(string phoneNumber) =>
        phoneNumber
            .RemoveWhitespace()
            .Validate();

    private static void Fail() => throw new ArgumentException();

    private static string Validate(this string result)
    {
        // Fail on any remaining invalid characters
        if (Regex.Match(result, @"[^0-9]").Success) Fail();

        // Invalid if length minus country code is 11+
        if (result.Length >= 11) Fail();
        if (result.Length <= 9) Fail();

        // Fail on area code beginning with zero or one
        if (result[0] == '0') Fail();
        if (result[0] == '1') Fail();

        // Fail on  exchange code beginning with zero or one
        if (result[3] == '0') Fail();
        if (result[3] == '1') Fail();

        return result;
    }

    private static string RemoveWhitespace(this string input)
    {
        // Remove allowed whitespace characters
        var result = input.Replace("(", "")
            .Replace(")", "")
            .Replace("-", "")
            .Replace("+", "")
            .Replace(".", "")
            .Replace(" ", "");

        // Remove country code
        if (result.Length == 11 && result[0] == '1') result = result.Substring(1);

        return result;
    }
}