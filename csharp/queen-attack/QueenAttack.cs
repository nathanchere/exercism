﻿using System;

public class Queen
{
    public Queen(int row, int column)
    {
        if (row < 0 || column < 0 || row > 7 || column > 7)
        {
            throw new ArgumentOutOfRangeException();
        }
        Row = row;
        Column = column;
    }

    public int Row { get; }
    public int Column { get; }
}

public static class QueenAttack
{
    public static bool CanAttack(Queen white, Queen black)
    {
        if (white.Column == black.Column) return true;
        if (white.Row == black.Row) return true;
        if (white.Row - white.Column == black.Row - black.Column) return true;
        if (white.Column - white.Row == black.Column - black.Row) return true;
        if (white.Row + white.Column == black.Row + black.Column) return true;
        if (white.Column + white.Row == black.Column + black.Row) return true;
        return false;
    }

    public static Queen Create(int row, int column) => new Queen(row, column);
}