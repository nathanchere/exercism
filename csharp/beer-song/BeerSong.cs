﻿using System;
using System.Text;

public static class BeerSong
{
    public const int ResetBottlesCount = 99;

    public static string Recite(int startBottles, int takeDown)
    {
        var result = new StringBuilder();
        var bottles = startBottles;
        var remaining = takeDown;

        while (remaining > 0)
        {
            if (bottles < 0)
            {
                bottles = ResetBottlesCount;
            }
            result.Append(Verse(bottles));
            bottles--;
            remaining--;
        }
        return result.ToString().TrimEnd();
    }

    private static string Verse(int bottles)
    {
        var bottlesMinusOne = bottles == 0
            ? ResetBottlesCount
            : bottles - 1;

        var nBottlesOfBeer = Lyrics.NBottlesOfBeer(bottles);
        var nMinusOneBottlesOfBeer = Lyrics.NBottlesOfBeer(bottlesMinusOne);
        var takeOneDown = Lyrics.TakeOneDown(bottles);

        return $"{nBottlesOfBeer} on the wall, {nBottlesOfBeer.ToLower()}.\n" +
               $"{takeOneDown}, {nMinusOneBottlesOfBeer.ToLower()} on the wall.\n\n";
    }

    private static class Lyrics
    {
        public static string NBottlesOfBeer(int bottles)
        {
            if (bottles == 0) return "No more bottles of beer";
            if (bottles == 1) return "1 bottle of beer";
            return $"{bottles} bottles of beer";
        }

        public static string TakeOneDown(int bottles)
        {
            if (bottles == 0) return "Go to the store and buy some more";
            if (bottles == 1) return "Take it down and pass it around";
            return "Take one down and pass it around";
        }
    }
}