using System;
using System.Collections.Concurrent;
using System.Text;

public class Robot
{
    private static readonly Random _random = new Random();
    private static readonly ConcurrentDictionary<string, bool> _reservedNames = new ConcurrentDictionary<string, bool>();
    private const uint MAX_POSSIBLE_NAMES = 26 * 26 * 10 * 10 * 10;

    public string Name { get; private set; }

    public Robot() => Reset();

    public void Reset()
    {
        if (_reservedNames.Count >= MAX_POSSIBLE_NAMES)
        {
            throw new InvalidOperationException("All available names already allocated");
        }

        do
        {
            var result = new StringBuilder(5);
            result.Append(GetLetter());
            result.Append(GetLetter());
            result.Append(GetNumber());
            result.Append(GetNumber());
            result.Append(GetNumber());
            Name = result.ToString();
        } while (_reservedNames.ContainsKey(Name));

        _reservedNames.TryAdd(Name, true);
    }

    private char GetLetter() => (char)(_random.Next('A', 'Z' + 1));
    private int GetNumber() => _random.Next(0, 10);
}