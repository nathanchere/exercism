using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

public static class PrimeFactors
{
    public static long[] Factors(long number)
    {
        if (number < 2)
        {
            return new long[0];
        }

        var primes = PrimeSieve.GetPrimes(number);
        var result = new List<long>();

        var remainder = number;
        foreach (var prime in primes)
        {
            if (remainder == 1)
            {
                return result.ToArray();
            }

            while (remainder % prime == 0)
            {
                result.Add((int)prime);
                remainder /= prime;
            }
        }

        return new[] { (long)number };
    }
}