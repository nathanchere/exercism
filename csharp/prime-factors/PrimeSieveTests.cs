using System;
using Xunit;
using System.Linq;

public class PrimeSieveTest
{
    [Fact]
    public void No_primes_for_1()
    {
        var result = PrimeSieve.GetPrimes(1);
        Assert.Empty(result);
    }

    [Fact]
    public void Single_prime_for_2()
    {
        var expected = 2;

        var result = PrimeSieve.GetPrimes(2).SingleOrDefault();

        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData(3L, 2L, 3L)]
    [InlineData(5L, 2L, 3L, 5L)]
    [InlineData(7L, 2L, 3L, 5L, 7L)]
    [InlineData(11L, 2L, 3L, 5L, 7L, 11L)]
    [InlineData(13L, 2L, 3L, 5L, 7L, 11L, 13L
    )]
    public void Returns_all_primes_up_to_and_including_prime_maxValue(
        long maxValue, params long[] expected)
    {
        var result = PrimeSieve.GetPrimes(maxValue).ToArray();

        Assert.Equal(expected, result);
    }

    [Theory]
    [InlineData(4L, 2L, 3L)]
    [InlineData(6L, 2L, 3L, 5L)]
    [InlineData(8L, 2L, 3L, 5L, 7L)]
    [InlineData(9L, 2L, 3L, 5L, 7L)]
    [InlineData(12L, 2L, 3L, 5L, 7L, 11L)]
    public void Returns_all_primes_up_to_nonprime_maxValue(
        long maxValue, params long[] expected)
    {
        var result = PrimeSieve.GetPrimes(maxValue).ToArray();

        Assert.Equal(expected, result);
    }

    [Fact]
    public void Returns_all_primes_for_large_maxValue()
    {
        const long expected = 894119L;

        var result = PrimeSieve.GetPrimes(expected + 1).Last();
        Assert.Equal(expected, result);
    }

    [Fact]
    public void Returns_all_primes_for_largest_maxValue_in_reasonable_time()
    {
        var result = PrimeSieve.GetPrimes(long.MaxValue);
        Assert.NotNull(result);
    }

    [Theory]
    [InlineData(3L, 0L)]
    [InlineData(5L, 1L)]
    [InlineData(7L, 2L)]
    [InlineData(9L, 3L)]
    [InlineData(11L, 4L)]
    [InlineData(13L, 5L)]
    public void Index_mapping_works_with_offset_and_no_even_numbers(long index, long expected)
    {
        var result = (long)((index - 3) * 0.5);
        Assert.Equal(expected, result);
    }
}