﻿using Xunit;

public class SpiralMatrixExtraTest
{
    [Fact]
    public void Correct_first_row_for_even_matrix()
    {
        var expectedFixture = Fixture.ForEvenMatrix();
        var expected = expectedFixture.GetRow(0);

        var result = Fixture.ForTestMatrix(expectedFixture.Size).GetRow(0);

        Assert.Equal(expected, result);
    }

    [Fact]
    public void Correct_last_row_for_even_matrix()
    {
        var expected = Fixture.ForEvenMatrix();
        var result = Fixture.ForTestMatrix(expected.Size);

        var rowIndex = expected.Size - 1;

        Assert.Equal(expected.GetRow(rowIndex), result.GetRow(rowIndex));
    }

    private class Fixture
    {
        public const int EvenMatrixSize = 6;
        public const int OddMatrixSize = 5;

        private static readonly int[,] EvenMatrix = new[,]{
            {  1,  2,  3,  4,  5,  6 },
            { 20, 21, 22 ,23, 24,  7 },
            { 19, 32, 33, 34, 25,  8 },
            { 18, 31, 36, 35, 26,  9 },
            { 17, 30, 29, 28, 27, 10 },
            { 16, 15, 14, 13, 12, 11 }
        };

        private static readonly int[,] OddMatrix = new[,] {
            {  1,  2,  3,  4,  5 },
            { 16, 17, 18, 19,  6 },
            { 15, 24, 25, 20,  7 },
            { 14, 23, 22, 21,  8 },
            { 13, 12, 11, 10,  9 }
        };

        private readonly int[,] Matrix;
        public readonly int Size;

        private Fixture(int size, int[,] matrix)
        {
            Size = size;
            Matrix = matrix;
        }

        public static Fixture ForTestMatrix(int size) => new Fixture(size, SpiralMatrix.GetMatrix(size));
        public static Fixture ForEvenMatrix() => new Fixture(EvenMatrixSize, EvenMatrix);
        public static Fixture ForOddMatrix() => new Fixture(OddMatrixSize, OddMatrix);

        public int[] GetRow(int rowIndex)
        {
            var result = new int[Size];
            for (int col = 0; col < Size; col++)
            {
                result[col] = Matrix[rowIndex, col];
            }
            return result;
        }

        public int[] GetCol(int colIndex)
        {
            var result = new int[Size];
            for (int row = 0; row < Size; row++)
            {
                result[row] = Matrix[row, colIndex];
            }
            return result;
        }
    }





}