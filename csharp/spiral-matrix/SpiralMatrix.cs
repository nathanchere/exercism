﻿using System;

public class SpiralMatrix
{
    public static int[,] GetMatrix(int size) => GetMatrixA(size);

    // Basic implementation
    private static int[,] GetMatrixA(int size)
    {
        var result = new int[size, size];

        var counter = 1;
        var total = size * size;
        var iterations = 0;

        while (counter <= total)
        {
            // Top-left to top-right
            for (int col = iterations; col < size - iterations; col++)
            {
                result[iterations, col] = counter++;
            }

            // Top-right to bottom-right
            for (int row = iterations + 1; row < size - iterations; row++)
            {
                result[row, size - iterations - 1] = counter++;
            }

            // Bottom-right to bottom-left
            for (int col = size - iterations - 2; col >= iterations; col--)
            {
                result[size - iterations - 1, col] = counter++;
            }

            // Bottom-left to top-left
            for (int row = size - iterations - 2; row > iterations; row--)
            {
                result[row, iterations] = counter++;
            }

            iterations++;
        }
        
        return result;
    }

    // Optimised for faster random access without generating entire matrix first
    private static int[,] GetMatrixB(int size)
    {
        var result = new int[size, size];
        for (int col = 0; col < size; col++)
        {
            result[0, col] = col + 1;
        }
        return result;
    }
}
