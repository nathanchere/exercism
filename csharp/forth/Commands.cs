public enum Command
{
    None = 0,
    Add,
    Subtract,
    Multiply,
    Divide,
    DUP,
    DROP,
    SWAP,
    OVER
}