﻿using System;
using System.Collections.Generic;
using System.Linq;

public interface IToken
{

}

public class ValueToken : IToken
{
    public int Value { get; }
    public ValueToken(int value)
    {
        Value = value;
    }

}
public class CommandToken : IToken
{
    public CommandToken(string command)
    {
        Command = command;
    }
    public string Command { get; }
}
public class DefinitionToken : IToken
{

}

public static class Forth
{
    public static string Evaluate(string[] input)
    {
        var context = new Context();
        var instructions = Parse(input);
        return null;
    }

    private static IEnumerable<IToken> Parse(string[] input)
    {
        foreach (var token in input.SelectMany(s => s.Split(" ", StringSplitOptions.RemoveEmptyEntries)))
        {
            if (Int32.TryParse(token, out var result))
            {
                yield return new ValueToken(result);
            }
            else if (token == ":")
            {
                yield return new DefinitionToken();
            }
            else
            {
                yield return new CommandToken(token);
            }
        }
    }
}

public class Context
{
    private readonly Dictionary<string, Operation[]> CommandRegister =
        new Dictionary<string, Operation[]> {
            {"+", new []{Operation.Add}},
            {"-", new []{Operation.Subtract}},
            {"*", new []{Operation.Multiply}},
            {"/", new []{Operation.Divide}},
            {"DUP", new []{Operation.Duplicate}},
            {"DROP", new []{Operation.Drop}},
            {"SWAP", new []{Operation.Swap}},
            {"OVER", new []{Operation.BringOver}},
        };

    private readonly Stack<int> Stack = new Stack<int>();

    private void Add() => Stack.Push(Stack.Pop() + Stack.Pop());
    private void Subtract() => Stack.Push(-Stack.Pop() + Stack.Pop());
    private void Multiply() => Stack.Push(Stack.Pop() * Stack.Pop());
    private void Divide() => Stack.Push((int)(1 / (Stack.Pop() / Stack.Pop())));
    private void Duplicate() => Stack.Push(Stack.Peek());
    private void Drop() => Stack.Pop();

    private void Swap()
    {
        var first = Stack.Pop();
        var second = Stack.Pop();
        Stack.Push(first);
        Stack.Push(second);
    }

    private void BringOver()
    {
        var first = Stack.Pop();
        var second = Stack.Peek();
        Stack.Push(first);
        Stack.Push(second);
    }

    public void Dispatch(Operation command)
    {
        try
        {
            switch (command)
            {
                case Operation.Add: Add(); break;
                case Operation.Subtract: Subtract(); break;
                case Operation.Multiply: Multiply(); break;
                case Operation.Divide: Divide(); break;
                case Operation.Duplicate: Duplicate(); break;
                case Operation.Drop: Drop(); break;
                case Operation.Swap: Swap(); break;
                case Operation.BringOver: BringOver(); break;
                default: throw new InvalidOperationException();
            }
        }
        catch (Exception)
        {
            throw new InvalidOperationException();
        }
    }

    public void Execute(IEnumerable<IToken> tokens)
    {
        foreach (var token in tokens)
        {
            switch (token)
            {
                case ValueToken value:
                    Stack.Push(value.Value);
                    break;
                case DefinitionToken definition:
                    break;
                case CommandToken command:
                    foreach (var operation in CommandRegister[command.Command])
                    {
                        Dispatch(operation);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}