using System;
using System.Collections.Generic;

public static class Etl
{
    public static Dictionary<string, int> Transform(Dictionary<int, string[]> old)
    {
        var result = new Dictionary<string, int>();
        foreach (var key in old.Keys)
        {
            foreach (var tile in old[key])
            {
                result[tile.ToLower()] = key;
            }
        }
        return result;
    }
}