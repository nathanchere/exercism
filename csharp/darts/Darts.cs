﻿using System;
using System.Linq;

public static class Darts
{
    private static readonly Ring[] _board;

    static Darts()
    {
        _board = new[] {
            new Ring(radius: 1, value: 10),
            new Ring(radius: 5, value: 5),
            new Ring(radius: 10, value: 1),
        };
    }

    public static int Score(double x, double y) =>
        _board.FirstOrDefault(ring => ring.IsHit(x, y))?.Value ?? 0;

    private class Ring
    {
        public Ring(double radius, int value)
        {
            _radiusSquared = radius;
            Value = value;
        }

        private readonly double _radiusSquared;
        public int Value { get; }

        public bool IsHit(double x, double y) =>
            (Math.Pow(x, 2) + Math.Pow(y, 2) <= Math.Pow(_radiusSquared, 2));

    }
}
