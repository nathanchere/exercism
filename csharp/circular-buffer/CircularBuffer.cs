﻿using System;

public class CircularBuffer<T>
{
    private readonly T[] _items;
    private readonly bool[] _itemFlags;
    private readonly int _capacity;
    private int _readIndex = 0;
    private int _writeIndex = 0;

    public CircularBuffer(int capacity)
    {
        _capacity = capacity;
        _items = new T[capacity];
        _itemFlags = new bool[capacity];
        Clear();
    }

    private int GetWriteIndex(bool overwrite = false)
    {
        for (int i = _writeIndex; i < _writeIndex + _capacity; i++)
        {
            var result = i % _capacity;
            if (_itemFlags[result] == false)
            {
                _writeIndex = result;
                return result;
            }
        }

        if (overwrite)
        {
            _writeIndex = _readIndex;
            GetReadIndex();
            return _writeIndex;
        }

        throw new InvalidOperationException($"Buffer is full");
    }

    private int GetReadIndex()
    {
        if (_itemFlags[_readIndex] == false)
        {
            throw new InvalidOperationException("Buffer is empty");
        }

        var result = _readIndex;
        _readIndex = (_readIndex + 1) % _capacity;
        return result;
    }

    public T Read()
    {
        var targetIndex = GetReadIndex();
        _itemFlags[targetIndex] = false;
        return _items[targetIndex];
    }

    public void Write(T value, bool overwrite = false)
    {
        var targetIndex = GetWriteIndex(overwrite);
        _items[targetIndex] = value;
        _itemFlags[targetIndex] = true;
    }

    public void Overwrite(T value) => Write(value, overwrite: true);

    public void Clear()
    {
        _readIndex = _writeIndex;

        for (int i = 0; i < _capacity; i++)
        {
            _itemFlags[i] = false;
        }
    }
}