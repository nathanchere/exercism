﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ProteinTranslation
{
    private const string EndOfSequence = "STOP";

    private static readonly Dictionary<string, string> _proteinMap = new Dictionary<string, string>
    {
        {"AUG","Methionine"},
        {"UUU","Phenylalanine"},
        {"UUC","Phenylalanine"},
        {"UUA","Leucine"},
        {"UUG","Leucine"},
        {"UCU","Serine"},
        {"UCC","Serine"},
        {"UCA","Serine"},
        {"UCG","Serine"},
        {"UAU","Tyrosine"},
        {"UAC","Tyrosine"},
        {"UGU","Cysteine"},
        {"UGC","Cysteine"},
        {"UGG","Tryptophan"},
        {"UAA",EndOfSequence},
        {"UAG",EndOfSequence},
        {"UGA",EndOfSequence},
    };

    public static string[] Proteins(string strand)
    {
        var result = new List<string>();

        for (int i = 0; i < strand.Length; i += 3)
        {
            var protein = _proteinMap[strand.Substring(i, 3)];
            if (protein == EndOfSequence) break;
            result.Add(protein);
        }

        return result.ToArray();
    }
}