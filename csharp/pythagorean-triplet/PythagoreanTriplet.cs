using System.Linq;
using System;
using System.Collections.Generic;

public static class PythagoreanTriplet
{
    public static IEnumerable<(int a, int b, int c)> TripletsWithSum(int sum)
    {
        for (int a = (int)Math.Sqrt(sum); a * 3 <= sum; a++)
        {
            for (int b = a + 1; b * 2 <= sum; b++)
            {
                int c = sum - a - b;

                if (a + b + c == sum && IsPythagoreanTriplet(a, b, c))
                {
                    yield return (a, b, c);
                }
            }
        }
    }

    private static bool IsPythagoreanTriplet(int a, int b, int c) => (a * a) + (b * b) == c * c;
}
