﻿using System;
using System.Linq;
using System.Collections.Generic;

public static class PascalsTriangle
{
    public static IEnumerable<IEnumerable<int>> Calculate(int rows)
    {
        if (rows == 0) return new Int32[0][];

        var result = new List<int[]> { new[] { 1 } };

        for (int row = 1; row < rows; row++)
        {
            var currentRow = new List<int> { 1 };
            for (int col = 1; col <= row - 1; col++)
            {
                var value = result[row - 1][col - 1] + result[row - 1][col];
                currentRow.Add(value);
            }
            currentRow.Add(1);
            result.Add(currentRow.ToArray());
        }
        return result.ToArray();
    }
}