using System;
using System.Collections.Generic;

public static class MatchingBrackets
{
    private readonly static Dictionary<char, char> BracketPairs;

    static MatchingBrackets()
    {
        BracketPairs = new Dictionary<char, char> {
            {'}','{'},
            {']','['},
            {')','('},
        };
    }

    public static bool IsPaired(string input)
    {
        var brackets = new Stack<char>();

        foreach (var c in input.ToCharArray())
        {
            if (BracketPairs.ContainsValue(c))
            {
                brackets.Push(c);
            }
            else if (BracketPairs.ContainsKey(c))
            {
                if (brackets.Count == 0 || brackets.Pop() != BracketPairs[c])
                {
                    return false;
                }
            }
        }

        return brackets.Count == 0;
    }
}
