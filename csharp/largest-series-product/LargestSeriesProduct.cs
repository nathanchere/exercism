using System;
using System.Linq;
using System.Collections.Generic;

public static class LargestSeriesProduct
{
    public static long GetLargestProduct(string digits, int span)
    {
        if (span < 0) throw new ArgumentException();
        if (span > digits.Length) throw new ArgumentException();
        if (!digits.All(char.IsDigit)) throw new ArgumentException();
        if (span == 0) return 1;

        long maxProduct = 0;
        var numbers = digits.Select(d => d - '0').ToList();

        for (int i = 0; i <= digits.Length - span; i++)
        {
            var product = Product(numbers.Skip(i).Take(span));
            maxProduct = Math.Max(maxProduct, product);
        }

        return maxProduct;
    }

    private static long Product(IEnumerable<int> digits)
    {
        var result = 1;
        foreach (var digit in digits)
        {
            result = result * digit;
        }
        return result;
    }
}