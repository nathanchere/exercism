﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

public static class Acronym
{
    public static string Abbreviate(string phrase)
    {
        var result = new StringBuilder();

        foreach (var token in Regex.Matches(Clean(phrase), @"\b[A-Z]"))
        {
            result.Append(token);
        }
        return result.ToString();
    }

    private static string Clean(string phrase)
    {
        var result = phrase.Replace("-", " ").ToUpper();
        return Regex.Replace(result, @"\b[^A-Z ]", "");
    }
}