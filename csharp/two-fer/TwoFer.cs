﻿using System;

public static class TwoFer
{
    public static string Name(string name = null) =>
      $"One for {name ?? "you"}, one for me.";
}
