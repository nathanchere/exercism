﻿using System;

public enum Direction
{
    North,
    East,
    South,
    West
}

public class RobotSimulator
{
    private const char CommandLeft = 'L';
    private const char CommandRight = 'R';
    private const char CommandAdvance = 'A';

    private Direction _direction;
    private (int X, int Y) _position;

    public RobotSimulator(Direction direction, int x, int y)
    {
        _direction = direction;
        _position = (x, y);
    }

    public Direction Direction { get => _direction; }
    public int X { get => _position.X; }
    public int Y { get => _position.Y; }

    private void RotateLeft()
    {
        _direction--;
        if (_direction < Direction.North) _direction = Direction.West;
    }

    private void RotateRight()
    {
        _direction++;
        if (_direction > Direction.West) _direction = Direction.North;
    }

    private void Advance()
    {
        switch (_direction)
        {
            case Direction.North:
                _position.Y++;
                break;
            case Direction.South:
                _position.Y--;
                break;
            case Direction.East:
                _position.X++;
                break;
            case Direction.West:
                _position.X--;
                break;
        }
    }

    public void Move(string instructions)
    {
        foreach (var instruction in instructions)
        {
            switch (instruction)
            {
                case CommandLeft:
                    RotateLeft();
                    break;
                case CommandRight:
                    RotateRight();
                    break;
                case CommandAdvance:
                    Advance();
                    break;
            }
        }
    }
}