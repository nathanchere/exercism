using System;
using System.Collections.Generic;
using System.Linq;

public static class RotationalCipher
{
    public static string Rotate(string text, int shiftKey)
        => text
            .Select(c => Rotate(c, shiftKey))
            .Stringify();

    private static char Rotate(char input, int shiftKey)
    {
        if (!char.IsLetter(input)) return input;

        var lower = char.IsUpper(input) ? 'A' : 'a';
        return (char)(lower + (input - lower + shiftKey) % 26);
    }

    private static string Stringify(this IEnumerable<char> input)
        => new String(input.ToArray());
}