﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ListOps
{
    public static int Length<T>(List<T> input)
    {
        var result = 0;
        input.ForEach(_item => result++);
        return result;
    }

    public static List<T> Reverse<T>(List<T> input)
    {
        var result = new List<T>();
        input.ForEach(item => result.Insert(0, item));
        return result;
    }

    public static List<TOut> Map<TIn, TOut>(List<TIn> input, Func<TIn, TOut> map) =>
        input.Select(item => map(item)).ToList();

    public static List<T> Filter<T>(List<T> input, Func<T, bool> predicate) =>
        input.Where(item => predicate(item)).ToList();

    public static TOut Foldl<TIn, TOut>(List<TIn> input, TOut start, Func<TOut, TIn, TOut> func)
    {
        var result = start;
        foreach (var item in input)
        {
            result = func(result, item);
        }
        return result;
    }

    public static TOut Foldr<TIn, TOut>(List<TIn> input, TOut start, Func<TIn, TOut, TOut> func)
    {
        var result = start;
        foreach (var item in Reverse(input))
        {
            result = func(item, result);
        }
        return result;
    }

    public static List<T> Concat<T>(List<List<T>> input) =>
        input.SelectMany(l => l).ToList();

    public static List<T> Append<T>(List<T> left, List<T> right)
    {
        var result = new List<T>();
        result.AddRange(left);
        result.AddRange(right);
        return result;
    }
}