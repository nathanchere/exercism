﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class SumOfMultiples
{
    public static int Sum(IEnumerable<int> multiples, int max)
    {
        var validMultiples = multiples.Where(m => m > 0);
        return GetMultiples(validMultiples, max).Sum();
    }

    private static IEnumerable<int> GetMultiples(IEnumerable<int> multiples, int max)
    {
        for (int i = 1; i < max; i++)
        {
            if (IsAnyMultiple(multiples, i)) yield return i;
        }
    }

    private static bool IsAnyMultiple(IEnumerable<int> multiples, int value)
    {
        foreach (var multiple in multiples)
        {
            if (value % multiple == 0) return true;
        }
        return false;
    }
}