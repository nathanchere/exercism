using System;
using System.Linq;

[Flags]
public enum Allergen
{
    None = 0,
    Eggs = 1,
    Peanuts = 2,
    Shellfish = 4,
    Strawberries = 8,
    Tomatoes = 16,
    Chocolate = 32,
    Pollen = 64,
    Cats = 128
}

public class Allergies
{
    private readonly int _allergiesMask;

    public Allergies(int mask) => _allergiesMask = mask;

    public bool IsAllergicTo(Allergen allergen) =>
        (_allergiesMask & (int)allergen) > 0;

    public Allergen[] List() =>
        Enum.GetValues(typeof(Allergen))
            .Cast<Allergen>()
            .Where(a => IsAllergicTo(a))
            .ToArray();
}