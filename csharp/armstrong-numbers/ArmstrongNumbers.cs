﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class ArmstrongNumbers
{
    public static bool IsArmstrongNumber(int number)
    {
        var digits = number.ToDigits();
        var power = digits.Length;
        return digits.Sum(i => (int)Math.Pow(i, power)) == number;
    }

    private static int[] ToDigits(this int number)
    {
        if (number == 0) return new[] { 0 };
        var result = new Stack<int>();
        while (number > 0)
        {
            result.Push(number % 10);
            number /= 10;
        };
        return result.ToArray();
    }
}