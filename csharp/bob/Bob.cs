using System;
using System.Linq;

public static class Bob
{
    public static string Response(string statement)
    {
        if (IsSilence(statement))
        {
            return "Fine. Be that way!";
        }

        var isYelling = IsYelling(statement);
        var isQuestion = IsQuestion(statement);

        if (isYelling && isQuestion)
            return "Calm down, I know what I'm doing!";
        if (isYelling)
            return "Whoa, chill out!";
        if (isQuestion)
            return "Sure.";
        return "Whatever.";
    }

    private static bool IsYelling(string statement) =>
        statement.Any(char.IsLetter) && !statement.Any(char.IsLower);

    private static bool IsQuestion(string statement) =>
        statement.TrimEnd().Last() == '?';

    private static bool IsSilence(string statement) =>
        string.IsNullOrWhiteSpace(statement);
}