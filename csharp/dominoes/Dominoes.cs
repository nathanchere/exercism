﻿using System.Collections.Generic;
using System.Linq;

public static class Dominoes
{
    public static bool CanChain(IEnumerable<(int, int)> input)
    {
        var dominoes = input.ToList();
        if (!dominoes.Any()) return true;

        // If any of the domino values appears an odd number of times, a full circular chain is not possible
        var values = dominoes.FlattenTuples();
        if (values.IsUnbalancedSet()) return false;

        // From this point we only care about unique values to speed up procesing
        dominoes = dominoes.Distinct().ToList();
        values = values.Distinct().ToList();

        // Starting at the first unique value, we'll track all other values which have a domino connecting it
        var connectedValues = new HashSet<int> { values.First() };
        values.RemoveAt(0);

        foreach (var value in values)
        {
            if (connectedValues.Contains(value)) continue;

            // If there are no dominoes joining the current value to any of the already connected values, a chain is not possible
            if (!dominoes.Any(d => d.ContainsAny(value, connectedValues))) return false;

            // Record any other values we can chain to from this value
            var matches = dominoes.Where(d => d.Contains(value)).ToList();
            foreach (var domino in matches)
            {
                connectedValues.Add(domino.Item1);
                connectedValues.Add(domino.Item2);
                dominoes.Remove(domino);
            }
        }

        // If we reach here, we've proven it is possible to create a circular chain from all of the dominoes provided
        return true;
    }

    // Indicates if a domino consists of one specific number AND one of various possible second numbers 
    private static bool ContainsAny(this (int, int) domino, int expected1, HashSet<int> expected2) =>
        domino.Item1 == expected1 && expected2.Contains(domino.Item2) ||
        expected2.Contains(domino.Item1) && domino.Item2 == expected1;

    // Indicates if a domino contains the expected face value on either side
    private static bool Contains(this (int, int) domino, int expected) =>
        domino.Item1 == expected || domino.Item2 == expected;

    private static List<int> FlattenTuples(this IEnumerable<(int, int)> dominoes) =>
        dominoes.SelectMany(d => new[] { d.Item1, d.Item2 }).ToList();

    // If any of the face values appear an uneven number of times, a full chain is not possible
    private static bool IsUnbalancedSet(this IEnumerable<int> dominoValues) =>
        dominoValues
            .GroupBy(i => i)
            .Select(grp => grp.Count())
            .Any(count => count % 2 != 0);
}
