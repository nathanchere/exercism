using System;
using System.Linq;
using System.Collections.Generic;

public static class NucleotideCount
{
    public static readonly List<char> AllNucleotides = new List<char> { 'A', 'G', 'C', 'T' };

    public static IDictionary<char, int> Count(string sequence)
    {
        var nucleotides = sequence.ToCharArray();

        if (nucleotides.Any(n => !AllNucleotides.Contains(n)))
        {
            throw new ArgumentException();
        }

        return AllNucleotides.ToDictionary(
            key => key,
            key => nucleotides.Count(n => n == key)
         );
    }
}