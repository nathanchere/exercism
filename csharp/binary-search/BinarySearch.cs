﻿using System;

public static class BinarySearch
{
    public static int Find(int[] input, int value)
    {
        var start = 0;
        var end = input.Length;

        while (start != end)
        {
            var index = start + (int)((end - start) * 0.5);
            var current = input[index];
            if (current == value)
            {
                return index;
            }

            if (end - start == 1)
            {
                start++;
            }
            else if (current > value)
            {
                end = index;
            }
            else
            {
                start = index;
            }
        }

        return -1;
    }
}