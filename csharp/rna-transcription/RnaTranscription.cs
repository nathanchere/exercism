using System;
using System.Linq;
using System.Collections.Generic;

public static class RnaTranscription
{
    public static string ToRna(string nucleotide)
    {
        var nucleotideMap = new Dictionary<char, char> {
            {'C', 'G'},
            {'G', 'C'},
            {'T', 'A'},
            {'A', 'U'},
        };

        return String.Concat(nucleotide
            .ToCharArray()
            .Select(n => nucleotideMap[n]));
    }
}