﻿using System.Linq;

public static class House
{
    public static string Recite(int verse)
    {
        var body = string.Join("", Enumerable.Range(1, verse - 1).Reverse()
            .Select(v => $" that {That(v)} the {The(v)}"));
        return $"This is the {The(verse)}{body}";
    }

    public static string Recite(int startVerse, int endVerse) =>
        string.Join("\n", Enumerable.Range(startVerse, 1 + endVerse - startVerse)
            .Select(i => Recite(i)));

    private static string The(int verse) => Verses[verse - 1].The;
    private static string That(int verse) => Verses[verse - 1].That;

    private static (string That, string The)[] Verses => new[] {
        ("lay in", "house that Jack built."),
        ("ate", "malt"),
        ("killed", "rat"),
        ("worried", "cat"),
        ("tossed", "dog"),
        ("milked", "cow with the crumpled horn"),
        ("kissed", "maiden all forlorn"),
        ("married", "man all tattered and torn"),
        ("woke", "priest all shaven and shorn"),
        ("kept", "rooster that crowed in the morn"),
        ("belonged to", "farmer sowing his corn"),
        ("", "horse and the hound and the horn"),
    };
}