using System;
using System.Linq;

public static class ScrabbleScore
{
    public static int Score(string input) =>
        input
            .ToUpperInvariant()
            .ToCharArray()
            .Select(TileToPoints)
            .Sum();

    private static int TileToPoints(char letter)
    {
        if ("AEIOULNRST".Contains(letter)) return 1;
        if ("DG".Contains(letter)) return 2;
        if ("BCMP".Contains(letter)) return 3;
        if ("FHVWY".Contains(letter)) return 4;
        if ('K' == letter) return 5;
        if ("JX".Contains(letter)) return 8;
        if ("QZ".Contains(letter)) return 10;
        throw new ArgumentException();
    }
}