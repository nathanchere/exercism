﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class Proverb
{
    public static string[] Recite(string[] subjects)
    {
        if (!subjects.Any()) return new string[0];

        var result = new List<string>();
        for (int i = 0; i < subjects.Length - 1; i++)
        {
            result.Add($"For want of a {subjects[i]} the {subjects[i + 1]} was lost.");
        }
        result.Add($"And all for the want of a {subjects[0]}.");
        return result.ToArray();
    }
}