﻿using System;
using System.Linq;

public static class Isogram
{
    public static bool IsIsogram(string word)
    {
        var chars = word.ToLowerInvariant()
            .ToCharArray()
            .Where(char.IsLetter)
            .ToArray();

        return chars.Distinct().Count() == chars.Length;
    }
}