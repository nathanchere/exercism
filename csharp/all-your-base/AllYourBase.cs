﻿using System;
using System.Collections.Generic;
using System.Linq;

public static class AllYourBase
{
    public static int[] Rebase(int inputBase, int[] inputDigits, int outputBase)
    {
        if (inputBase < 2 || outputBase < 2)
        {
            throw new ArgumentException("Negative base not supported");
        }

        var input = ToDecimal(inputBase, inputDigits);
        var result = new Stack<int>();

        do
        {
            result.Push(input % outputBase);
            input /= outputBase;
        }
        while (input > 0);

        return result.ToArray();
    }

    public static int ToDecimal(int inputBase, int[] digits)
    {
        var result = 0;
        for (int i = 0; i < digits.Length; i++)
        {
            var digit = digits[i];
            if (digit < 0) throw new ArgumentException("Negative digits not supported");
            if (digit >= inputBase) throw new ArgumentException($"Input digit {digit} invalid for input base {inputBase}");
            result += (int)Math.Pow(inputBase, digits.Length - i - 1) * digit;
        }
        return result;
    }
}