defmodule Sublist do
  @doc """
  Returns whether the first list is a sublist or a superlist of the second list
  and if not whether it is equal or unequal to the second list.
  """
  def compare(a, b) when length(a) == length(b) and a == b, do: :equal
  def compare(a, b) when length(a) == length(b), do: :unequal
  def compare(a, b) when length(a) > length(b), do: compare(b, a, :superlist)
  def compare(a, b) when length(a) < length(b), do: compare(a, b, :sublist)

  defp compare(a, [], _), do: :unequal

  defp compare(a, [_ | tail] = b, type) do
    cond do
      :lists.prefix(a, b) -> type
      true -> compare(a, tail, type)
    end
  end
end
