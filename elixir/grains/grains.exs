defmodule Grains do
  use Bitwise

  @error_out_of_range {:error, "The requested square must be between 1 and 64 (inclusive)"}

  @doc """
  Calculate two to the power of the input minus one.
  """
  @spec square(pos_integer) :: pos_integer
  def square(number) when number >= 1 and number <= 64, do: {:ok, calculate_grains(number)}
  def square(_), do: @error_out_of_range

  @doc """
  Adds square of each number from 1 to 64.
  """
  @spec total :: pos_integer
  def total, do: {:ok, calculate_grains(65) - 1}

  defp calculate_grains(square), do: 1 <<< (square - 1)
end
