defmodule Context do
  defstruct [:stack, :words]

  @initial_words %{
    "+" => [:add],
    "-" => [:subtract],
    "*" => [:multiply],
    "/" => [:divide],
    "DROP" => [:drop],
    "DUP" => [:duplicate],
    "OVER" => [:over],
    "SWAP" => [:swap]
  }

  def new(), do: %Context{stack: [], words: @initial_words}

  def execute(%Context{words: words} = context, {:definition, word, definition}) do
    commands = translate_commands(words, word, definition, [])
    new_words = Map.put(words, word, commands)

    %{context | words: new_words}
  end

  def execute(%Context{stack: stack} = context, {:number, value}),
    do: %{context | stack: [value] ++ stack}

  def execute(%Context{words: words, stack: stack} = context, {:command, word}) do
    case Map.get(words, String.upcase(word), :unknown) do
      :unknown -> raise Forth.UnknownWord
      result -> execute_commands(context, stack, result)
    end
  end

  defp execute_commands(context, stack, []), do: %{context | stack: stack}

  defp execute_commands(context, stack, [value | t]) when is_integer(value),
    do: execute_commands(context, [value] ++ stack, t)

  defp execute_commands(context, [a, b | stack], [:add | t]),
    do: execute_commands(context, [a + b] ++ stack, t)

  defp execute_commands(context, [a, b | stack], [:subtract | t]),
    do: execute_commands(context, [b - a] ++ stack, t)

  defp execute_commands(context, [a, b | stack], [:multiply | t]),
    do: execute_commands(context, [b * a] ++ stack, t)

  defp execute_commands(_, [0, _ | _], [:divide | _]), do: raise(Forth.DivisionByZero)

  defp execute_commands(context, [a, b | stack], [:divide | t]),
    do: execute_commands(context, [trunc(b / a)] ++ stack, t)

  defp execute_commands(context, [_ | stack], [:drop | t]),
    do: execute_commands(context, stack, t)

  defp execute_commands(context, [a | _] = stack, [:duplicate | t]),
    do: execute_commands(context, [a] ++ stack, t)

  defp execute_commands(context, [_, b | _] = stack, [:over | t]),
    do: execute_commands(context, [b] ++ stack, t)

  defp execute_commands(context, [a, b | stack], [:swap | t]),
    do: execute_commands(context, [b, a] ++ stack, t)

  defp execute_commands(_, _, _), do: raise(Forth.StackUnderflow)

  defp translate_commands(_, word, [word | _], _), do: raise(Forth.RecursiveDefinition)
  defp translate_commands(_, _, [], result), do: result

  defp translate_commands(words, word, [h | t], result) when is_integer(h),
    do: translate_commands(words, word, t, [h] ++ result)

  defp translate_commands(words, word, [h | t], result) do
    case Map.get(words, h, :notfound) do
      :notfound ->
        raise Forth.UnknownWord

      commands ->
        translate_commands(words, word, t, Enum.reverse(commands) ++ result)
    end
  end
end
