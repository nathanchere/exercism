Code.load_file("parser.exs", __DIR__)
Code.load_file("context.exs", __DIR__)

defmodule Forth do
  @opaque evaluator :: Context.t()

  @doc """
  Create a new evaluator.
  """
  @spec new() :: evaluator
  def new() do
    Context.new()
  end

  @doc """
  Evaluate an input string, updating the evaluator state.
  """
  @spec eval(evaluator, String.t()) :: evaluator
  def eval(ev, input) do
    Regex.scan(~r/[^\pC\s]+/u, input)
    |> List.flatten()
    |> Parser.parse()
    |> Enum.reduce(ev, fn token, acc -> Context.execute(acc, token) end)
  end

  @doc """
  Return the current stack as a string with the element on top of the stack
  being the rightmost element in the string.
  """
  @spec format_stack(evaluator) :: String.t()
  def format_stack(%Context{stack: stack}) do
    stack
    |> Enum.reverse()
    |> Enum.join(" ")
  end

  defmodule RecursiveDefinition do
    defexception []
    def message(_), do: "recursive word definition"
  end

  defmodule InvalidSyntax do
    defexception []
    def message(_), do: "invalid syntax"
  end

  defmodule StackUnderflow do
    defexception []
    def message(_), do: "stack underflow"
  end

  defmodule InvalidWord do
    defexception word: nil
    def message(e), do: "invalid word: #{inspect(e.word)}"
  end

  defmodule UnknownWord do
    defexception word: nil
    def message(e), do: "unknown word: #{inspect(e.word)}"
  end

  defmodule DivisionByZero do
    defexception []
    def message(_), do: "division by zero"
  end
end
