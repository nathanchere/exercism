defmodule Parser do
  def parse(tokens), do: parse(tokens, [])

  defp parse([], result), do: result |> Enum.reverse()

  defp parse([":", word | t], result) do
    case Integer.parse(word) do
      :error -> parse_definition(String.upcase(word), t, [], result)
      # numbers cannot be redefined
      _ -> raise Forth.InvalidWord
    end
  end

  defp parse([token | t], result) do
    case Integer.parse(token) do
      {value, _} -> parse(t, [{:number, value}] ++ result)
      _ -> parse(t, [{:command, token}] ++ result)
    end
  end

  defp parse_definition(_, [], _, _) do
    # Must end word definitions with a semi-colon
    raise Forth.InvalidSyntax
  end

  defp parse_definition(_, [":" | _], _, _) do
    # Nested word definitions (pseudo-macros?) are not supported
    raise Forth.InvalidSyntax
  end

  defp parse_definition(word, [";" | t], definition, result) do
    parse(t, [{:definition, word, definition}] ++ result)
  end

  defp parse_definition(word, [h | t], definition, result) do
    case Integer.parse(h) do
      {value, _} -> parse_definition(word, t, [value] ++ definition, result)
      _ -> parse_definition(word, t, [String.upcase(h)] ++ definition, result)
    end
  end
end
