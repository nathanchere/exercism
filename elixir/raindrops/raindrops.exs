defmodule Raindrops do
  @spec convert(pos_integer) :: String.t()
  def convert(number) do
    {number, ""}
    |> check_drop(rem(number, 3) == 0, "Pling")
    |> check_drop(rem(number, 5) == 0, "Plang")
    |> check_drop(rem(number, 7) == 0, "Plong")
    |> format
  end

  defp check_drop({number, result}, true, drop), do: {number, result <> drop}
  defp check_drop(input, _, _), do: input

  defp format({number, ""}), do: "#{number}"
  defp format({_, drops}), do: drops
end
