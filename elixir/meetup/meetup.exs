defmodule Meetup do
  @moduledoc """
  Calculate meetup dates.
  """

  @type weekday :: :monday | :tuesday | :wednesday | :thursday | :friday | :saturday | :sunday
  @type schedule :: :first | :second | :third | :fourth | :last | :teenth

  @doc """
  Calculate a meetup date.

  The schedule is in which week (1..4, last or "teenth") the meetup date should
  fall.
  """
  @spec meetup(pos_integer, pos_integer, weekday, schedule) :: :calendar.date()
  def meetup(year, month, weekday, schedule) do
    get_days_in_month
    |> filter_on_weekday(weekday)
    |> filter_on_schedule(schedule)
    |> Enum.at(0)
    |> to_result
  end

  defp get_days_in_month(year, month) do
    {_, from} = Date.new(year, month, 1)
    {_, to} = Date.new(year, month, Date.days_in_month(from))
    Date.range(from, to)
  end

  defp to_result(date), do: {date.year, date.month, date.day}

  defp filter_on_weekday(candidates, weekday) do
    dayofweek =
      case weekday do
        :monday -> 1
        :tuesday -> 2
        :wednesday -> 3
        :thursday -> 4
        :friday -> 5
        :saturday -> 6
        :sunday -> 7
      end

    Enum.filter(candidates, fn x -> Date.day_of_week(x) == dayofweek end)
  end

  defp filter_on_schedule(candidates, :first), do: candidates
  defp filter_on_schedule(candidates, :second), do: Enum.drop(candidates, 1)
  defp filter_on_schedule(candidates, :third), do: Enum.drop(candidates, 2)
  defp filter_on_schedule(candidates, :fourth), do: Enum.drop(candidates, 3)
  defp filter_on_schedule(candidates, :last), do: Enum.reverse(candidates)

  defp filter_on_schedule(candidates, :teenth),
    do: Enum.filter(candidates, fn d -> d.day() > 12 end)
end
