defmodule Roman do
  @glyphs [
    {1000, "M"},
    {900, "CM"},
    {500, "D"},
    {400, "CD"},
    {100, "C"},
    {90, "XC"},
    {50, "L"},
    {40, "XL"},
    {10, "X"},
    {9, "IX"},
    {5, "V"},
    {4, "IV"},
    {1, "I"}
  ]

  @doc """
  Convert the number to a Roman number.
  """
  @spec numerals(pos_integer) :: String.t()
  def numerals(decimal), do: encode(decimal, @glyphs, "")

  @doc """
  Convert Roman number to a number.
  """
  @spec decimal(String.t()) :: Integer.t()
  def decimal(numerals) do
    numerals
    |> String.graphemes()
    |> Enum.map(&roman_to_decimal/1)
    |> decode
  end

  defp encode(_, [], result), do: result

  defp encode(remaining, [{value, glyph} | _] = glyphs, result) when remaining >= value,
    do: encode(remaining - value, glyphs, result <> glyph)

  defp encode(remaining, [_ | t], result), do: encode(remaining, t, result)

  defp decode(digits), do: decode(digits, 0)
  defp decode([], result), do: result

  defp decode([a, b | tail], result) when a < b do
    decode(tail, result + b - a)
  end

  defp decode([a | tail], result) do
    decode(tail, result + a)
  end

  @spec roman_to_decimal(char) :: Integer.t()
  defp roman_to_decimal(digit) do
    case digit do
      "I" -> 1
      "V" -> 5
      "X" -> 10
      "L" -> 50
      "C" -> 100
      "D" -> 500
      "M" -> 1000
    end
  end
end
