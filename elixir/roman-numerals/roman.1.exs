defmodule Roman do
  @doc """
  Convert the number to a Roman number.
  """
  @spec numerals(pos_integer) :: String.t()
  def numerals(decimal), do: encode(decimal, "")

  @doc """
  Convert Roman number to a number.
  """
  @spec decimal(String.t()) :: Integer.t()
  def decimal(numerals) do
    numerals
    |> String.graphemes()
    |> Enum.map(&roman_to_decimal/1)
    |> decode
  end

  defp encode(0, result), do: result
  defp encode(value, result) when value >= 1000, do: encode(value - 1000, result <> "M")
  defp encode(value, result) when value >= 900, do: encode(value - 900, result <> "CM")
  defp encode(value, result) when value >= 500, do: encode(value - 500, result <> "D")
  defp encode(value, result) when value >= 400, do: encode(value - 400, result <> "CD")
  defp encode(value, result) when value >= 100, do: encode(value - 100, result <> "C")
  defp encode(value, result) when value >= 90, do: encode(value - 90, result <> "XC")
  defp encode(value, result) when value >= 50, do: encode(value - 50, result <> "L")
  defp encode(value, result) when value >= 40, do: encode(value - 40, result <> "XL")
  defp encode(value, result) when value >= 10, do: encode(value - 10, result <> "X")
  defp encode(value, result) when value >= 9, do: encode(value - 9, result <> "IX")
  defp encode(value, result) when value >= 5, do: encode(value - 5, result <> "V")
  defp encode(value, result) when value >= 4, do: encode(value - 4, result <> "IV")
  defp encode(value, result) when value >= 1, do: encode(value - 1, result <> "I")

  defp decode(digits), do: decode(digits, 0)
  defp decode([], result), do: result

  defp decode([a, b | tail], result) when a < b do
    decode(tail, result + b - a)
  end

  defp decode([a | tail], result) do
    decode(tail, result + a)
  end

  @spec roman_to_decimal(char) :: Integer.t()
  defp roman_to_decimal(digit) do
    case digit do
      "I" -> 1
      "V" -> 5
      "X" -> 10
      "L" -> 50
      "C" -> 100
      "D" -> 500
      "M" -> 1000
    end
  end
end
