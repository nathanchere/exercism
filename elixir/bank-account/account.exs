defmodule AccountStatus do
  defstruct [:status, :balance]

  def new(status, balance) do
    %AccountStatus{status: status, balance: balance}
  end

  def new() do
    %AccountStatus{status: :account_open, balance: 0}
  end
end

defmodule BankAccount do
  @moduledoc """
  A bank account that supports access from multiple processes.
  """

  @typedoc """
  An account handle.
  """
  @opaque account :: pid

  @error_account_closed {:error, :account_closed}
  @error_account_not_exist {:error, :account_not_exist}

  @doc """
  Open the bank. Makes the account available.
  """
  @spec open_bank() :: account
  def open_bank() do
    {:ok, account} = Agent.start_link(fn -> AccountStatus.new() end)
    account
  end

  @doc """
  Close the bank. Makes the account unavailable.
  """
  @spec close_bank(account) :: none
  def close_bank(account), do: close_bank(account, does_account_exist?(account))

  defp close_bank(account, _account_exists = true) do
    Agent.update(account, fn _ -> AccountStatus.new(:account_closed, nil) end)
  end

  defp close_bank(_, _account_exists = false) do
    @error_account_not_exist
  end

  @doc """
  Get the account's balance.
  """
  @spec balance(account) :: integer
  def balance(account), do: balance(account, does_account_exist?(account))

  defp balance(account, _account_exists = true) do
    %AccountStatus{status: status, balance: balance} = Agent.get(account, fn state -> state end)

    case status do
      :account_open -> balance
      :account_closed -> @error_account_closed
    end
  end

  defp balance(_, _account_exists = false), do: @error_account_not_exist

  @doc """
  Update the account's balance by adding the given amount which may be negative.
  """
  @spec update(account, integer) :: any
  def update(account, amount), do: update(account, amount, does_account_exist?(account))

  defp update(account, amount, _account_exists = true) do
    %AccountStatus{status: status, balance: balance} = Agent.get(account, fn state -> state end)

    case status do
      :account_open ->
        new_status = AccountStatus.new(status, balance + amount)
        Agent.update(account, fn _ -> new_status end)

      :account_closed ->
        @error_account_closed
    end
  end

  defp update(_, _, _account_exists = false), do: @error_account_not_exist

  defp does_account_exist?(account), do: Process.alive?(account)
end
