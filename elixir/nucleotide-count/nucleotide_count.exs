defmodule NucleotideCount do
  @nucleotides %{?A => 0, ?T => 0, ?C => 0, ?G => 0}

  @spec histogram([char]) :: map
  def histogram(strand) do
    Enum.reduce(strand, @nucleotides, &count_strand/2)
  end

  defp count_strand(strand, result), do: Map.update(result, strand, 1, &(&1 + 1))

  def count(strand, result) do
    strand
    |> to_charlist
    |> Enum.filter(&(result == &1))
    |> length
  end
end
