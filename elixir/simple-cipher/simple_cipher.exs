defmodule SimpleCipher do
  def encode(plaintext, key), do: process(plaintext, parse_encode_key(key))
  def decode(ciphertext, key), do: process(ciphertext, parse_decode_key(key))

  def process(input, key),
    do:
      input
      |> String.to_charlist()
      |> process(key, key, [])
      |> to_string

  defp process([], _, _, result), do: result |> Enum.reverse()
  defp process(input, [], key, result), do: process(input, key, key, result)

  defp process([h | t], [hkey | tkey], key, result) do
    c = cycle(h, hkey)
    process(t, tkey, key, [c] ++ result)
  end

  defp parse_encode_key(key),
    do:
      key
      |> String.to_charlist()
      |> Enum.map(fn c -> c - ?a end)

  defp parse_decode_key(key),
    do:
      key
      |> String.to_charlist()
      |> Enum.map(fn c -> 0 - (c - ?a) end)

  defp cycle(c, _offset) when c < ?a, do: c
  defp cycle(c, _offset) when c > ?z, do: c
  defp cycle(c, offset) when c + offset < ?a, do: c + offset + 26
  defp cycle(c, offset) when c + offset > ?z, do: c + offset - 26
  defp cycle(c, offset), do: c + offset
end
