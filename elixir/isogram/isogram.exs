defmodule Isogram do
  @doc """
  Determines if a word or sentence is an isogram
  """
  @spec isogram?(String.t()) :: boolean
  def isogram?(sentence) do
    sentence
    |> get_letters
    |> is_unique?
  end

  defp get_letters(string),
    do:
      Regex.replace(~r/[^A-Za-z]/, string, "")
      |> String.graphemes()

  defp is_unique?(input), do: length(input) == length(Enum.uniq(input))
end
