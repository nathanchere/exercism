defmodule Clock do
  defstruct hour: 0, minute: 0

  @spec new(integer, integer) :: Clock
  def new(hour, minute) do
    h = hour + minutes_to_hour_offset(minute)
    %Clock{hour: normalize(h, 24), minute: normalize(minute, 60)}
  end

  @spec add(Clock, integer) :: Clock
  def add(%Clock{hour: hour, minute: minute}, add_minute), do: new(hour, minute + add_minute)

  defp minutes_to_hour_offset(minute) when minute < 0, do: div(minute, 60) - 1
  defp minutes_to_hour_offset(minute), do: div(minute, 60)

  defp normalize(value, max), do: rem(rem(value, max) + max, max)

  defimpl String.Chars, for: Clock do
    def to_string(%Clock{hour: h, minute: m}),
      do: Kernel.to_string(:io_lib.format("~2..0B:~2..0B", [h, m]))
  end
end
