defmodule ListOps do
  @spec count(list) :: non_neg_integer
  def count(l) do
    _count(l, 0)
  end

  defp _count([], total), do: total
  defp _count([_ | t], total), do: _count(t, total + 1)

  @spec reverse(list) :: list
  def reverse(l) do
    _reverse(l, [])
  end

  defp _reverse([], result), do: result
  defp _reverse([h | t], result), do: _reverse(t, [h | result])

  @spec map(list, (any -> any)) :: list
  def map(l, f) do
    _map(l, f, [])
    |> reverse
  end

  defp _map([], _, result), do: result
  defp _map([h | t], f, result), do: _map(t, f, [f.(h) | result])

  @spec filter(list, (any -> as_boolean(term))) :: list
  def filter(l, f) do
    _filter(l, f, [])
    |> reverse
  end

  defp _filter([], _, result), do: result

  defp _filter([h | t], f, result) do
    next = (f.(h) && [h | result]) || result
    _filter(t, f, next)
  end

  def reduce(l, acc, f) do
    _reduce(l, acc, f)
  end

  defp _reduce([], acc, _), do: acc
  defp _reduce([h | t], acc, f), do: _reduce(t, f.(h, acc), f)

  @spec append(list, list) :: list
  def append(a, b) do
    reverse(a)
    |> _append(b)
  end

  defp _append([], result), do: result
  defp _append([h | t], result), do: _append(t, [h | result])

  @spec concat([[any]]) :: [any]
  def concat(ll) do
    reverse(ll)
    |> _concat([])
  end

  defp _concat([], result), do: result
  defp _concat([h | t], result), do: _concat(t, append(h, result))
end
