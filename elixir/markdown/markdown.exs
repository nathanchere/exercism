defmodule Markdown do
  @spec parse(String.t()) :: String.t()
  def parse(text) do
    text
    |> String.split("\n")
    |> Enum.map_join(&process_line/1)
    |> cleanup_lists
  end

  defp process_line(line) do
    [header | words] = String.split(line)
    body = Enum.join(words, " ")

    cond do
      header =~ ~r/^#+$/ -> process_header(header, body)
      header == "*" -> process_list(body)
      true -> process_paragraph(line)
    end
  end

  defp process_header(tag, body) do
    level = String.length(tag)
    body |> wrap_in_tag("h#{level}")
  end

  defp process_list(body) do
    body
    |> replace_inline_tags
    |> wrap_in_tag("li")
    |> wrap_in_tag("ul")
  end

  defp process_paragraph(body) do
    body
    |> replace_inline_tags
    |> wrap_in_tag("p")
  end

  defp replace_inline_tags(body) do
    body
    |> String.replace(~r/___(.*)___/, "<em><strong>\\1</strong></em>")
    |> String.replace(~r/__(.*)__/, "<strong>\\1</strong>")
    |> String.replace(~r/_(.*)_/, "<em>\\1</em>")
  end

  defp cleanup_lists(text), do: String.replace(text, "</ul><ul>", "")

  defp wrap_in_tag(content, tag), do: "<#{tag}>#{content}</#{tag}>"
end
