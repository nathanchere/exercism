defmodule Frame do
  defstruct [:number, :rolls, :is_complete?]

  @error_too_many_pins {:error, "Pin count exceeds pins on the lane"}
  @error_negative_pins {:error, "Negative roll is invalid"}
  @error_frame_already_complete {:error, "Frame already complete"}

  def new(number),
    do: %Frame{
      number: number,
      is_complete?: false,
      rolls: []
    }

  def add_roll(%Frame{is_complete?: true}, _),
    do: @error_frame_already_complete

  def add_roll(_, pins)
      when pins < 0,
      do: @error_negative_pins

  def add_roll(_, pins)
      when pins > 10,
      do: @error_too_many_pins

  def add_roll(%Frame{rolls: [10, b2]}, pins)
      when b2 < 10 and b2 + pins > 10,
      do: @error_too_many_pins

  def add_roll(%Frame{rolls: [b1]}, pins)
      when b1 < 10 and b1 + pins > 10,
      do: @error_too_many_pins

  # special rules for the 10th frame

  def add_roll(%Frame{number: 10, rolls: []} = frame, pins) do
    {:ok, %{frame | rolls: [pins]}}
  end

  def add_roll(%Frame{number: 10, rolls: [b1]} = frame, pins) do
    {:ok, %{frame | rolls: [b1, pins], is_complete?: b1 != 10 and b1 + pins != 10}}
  end

  def add_roll(%Frame{number: 10, rolls: [b1, b2]} = frame, pins) do
    {:ok, %{frame | rolls: [b1, b2, pins], is_complete?: true}}
  end

  # standard frame

  def add_roll(%Frame{rolls: []} = frame, 10) do
    {:ok, %{frame | rolls: [10], is_complete?: true}}
  end

  def add_roll(%Frame{rolls: []} = frame, pins) do
    {:ok, %{frame | rolls: [pins]}}
  end

  def add_roll(%Frame{rolls: [b1]} = frame, pins) do
    {:ok, %{frame | rolls: [b1, pins], is_complete?: true}}
  end
end
