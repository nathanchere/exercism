Code.load_file("frame.exs", __DIR__)

defmodule Game do
  defstruct [:frames, :current_frame, :is_complete?]

  def new do
    %Game{frames: [], current_frame: Frame.new(1), is_complete?: false}
  end

  def update_frame(%Game{is_complete?: true}) do
    {:error, "Cannot roll after game is over"}
  end

  def update_frame(
        %Game{frames: frames} = game,
        %Frame{is_complete?: true, number: 10} = frame
      ) do
    %{game | frames: [frame] ++ frames, current_frame: nil, is_complete?: true}
  end

  def update_frame(
        %Game{frames: frames} = game,
        %Frame{is_complete?: true, number: number} = frame
      ) do
    %{game | frames: [frame] ++ frames, current_frame: Frame.new(number + 1)}
  end

  def update_frame(%Game{} = game, frame) do
    %{game | current_frame: frame}
  end
end
