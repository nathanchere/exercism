Code.load_file("score.exs", __DIR__)

defmodule Bowling do
  @error_incomplete_game {:error, "Score cannot be taken until the end of the game"}
  @error_complete_game {:error, "Cannot roll after game is over"}

  @doc """
    Creates a new game of bowling that can be used to store the results of
    the game
  """
  @spec start() :: Game.t()
  def start do
    Game.new()
  end

  @doc """
    Records the number of pins knocked down on a single roll. Returns `any`
    unless there is something wrong with the given number of pins, in which
    case it returns a helpful message.
  """
  @spec roll(Game.t(), integer) :: Game.t()
  def roll(%Game{is_complete?: true}, _), do: @error_complete_game

  def roll(%Game{current_frame: current_frame} = game, pins) do
    {result, body} = current_frame |> Frame.add_roll(pins)

    case result do
      :error -> {:error, body}
      :ok -> Game.update_frame(game, body)
    end
  end

  @doc """
    Returns the score of a given game of bowling if the game is complete.
    If the game isn't complete, it returns a helpful message.
  """
  @spec score(Game.t()) :: integer | String.t()
  def score(%Game{is_complete?: false} = game) do
    @error_incomplete_game
  end

  def score(%Game{frames: frames}) do
    Score.process(frames)
  end
end
