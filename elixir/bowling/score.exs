Code.load_file("game.exs", __DIR__)

defmodule Score do
  def process(frames) do
    frames
    |> Enum.map(&mapper/1)
    |> List.flatten()
    |> reduce(nil, nil, 0)
  end

  defp mapper(%Frame{rolls: rolls, number: 10}),
    do: rolls |> Enum.reverse()

  defp mapper(%Frame{rolls: [10]}),
    do: [{:strike, 10}]

  defp mapper(%Frame{rolls: [b1, b2]}) when b1 + b2 == 10,
    do: [{:spare, b2}, b1]

  defp mapper(%Frame{rolls: rolls}),
    do: rolls |> Enum.reverse()

  defp reduce([], _, _, total), do: total

  defp reduce([{:strike, _} | t], last, lastlast, total),
    do: reduce(t, 10, last, total + 10 + last + lastlast)

  defp reduce([{:spare, pins} | t], last, _lastlast, total),
    do: reduce(t, pins, last, total + pins + last)

  defp reduce([pins | t], last, _lastlast, total),
    do: reduce(t, pins, last, total + pins)
end
