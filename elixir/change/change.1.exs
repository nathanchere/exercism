defmodule Change do
  @error {:error, "cannot change"}

  @spec generate(list, integer) :: {:ok, list} | {:error, String.t()}
  def generate(_, target) when target < 0, do: @error

  def generate(coins, target) do
    coins
    |> get_coin_permutations()
    |> Enum.reduce(@error, fn coins, acc -> reduce_change(coins, acc, target) end)
  end

  defp reduce_change(coins, {old_result, old_coins} = acc, target) do
    {new_result, new_coins} = calculate_change(coins, target, [])

    cond do
      new_result == :error ->
        acc

      old_result == :error ->
        {new_result, new_coins}

      length(new_coins) < length(old_coins) ->
        {new_result, new_coins}

      true ->
        acc
    end
  end

  def get_coin_permutations([]), do: [[]]

  def get_coin_permutations(coins) do
    for h <- coins, t <- get_coin_permutations(coins -- [h]), do: [h | t]
  end

  defp calculate_change([], 0, change), do: {:ok, change}
  defp calculate_change([], _, _), do: @error

  defp calculate_change([largest_coin | coins], target, change)
       when largest_coin <= target do
    calculate_change([largest_coin | coins], target - largest_coin, [
      largest_coin | change
    ])
  end

  defp calculate_change([_ | coins], target, change) do
    calculate_change(coins, target, change)
  end
end
