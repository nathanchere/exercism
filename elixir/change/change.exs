defmodule Change do
  @error {:error, "cannot change"}

  @spec generate(list, integer) :: {:ok, list} | {:error, String.t()}
  def generate(_, target) when target < 0, do: @error
  def generate(_, 0), do: []

  def generate(coins, target) do
    coins
    |> sort_available_coins
    |> get_permutations(target)

    # |> Enum.reduce(@error, fn coins, acc -> reduce_change(coins, acc, target) end)
  end

  defp sort_available_coins(coins),
    do:
      coins
      |> Enum.uniq()
      |> Enum.sort(&(&1 >= &2))

  ### Example:
  # iex(1)> Change.filter_available_coins([5,3,1])
  # [{5, [5, 3, 1]}, {3, [3, 1]}, {1, [1]}]

  def filter_available_coins(coins), do: filter_available_coins(coins, [])
  defp filter_available_coins([], result), do: result

  defp filter_available_coins([h | t] = available_coins, result) do
    new_change_combination = {h, available_coins}
    [new_change_combination | filter_available_coins(t, result)]
  end

  defp get_permutations(available_coins, target) do
    for {coin, remaining} <- filter_available_coins(available_coins),
        do: get_permutations(remaining, target - coin, [coin])
  end

  defp get_permutations(_, 0, result), do: result
  defp get_permutations(_, target, _) when target < 0, do: nil

  defp get_permutations(available_coins, target, result) do
    for {coin, remaining} <- filter_available_coins(available_coins),
        do: get_permutations(remaining, target - coin, [coin | result])
  end

end
