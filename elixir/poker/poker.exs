Code.load_file("card.exs", __DIR__)
Code.load_file("hand.exs", __DIR__)
Code.load_file("sorted_hand.exs", __DIR__)

defmodule Poker do
  @spec best_hand(list(list(String.t()))) :: list(list(String.t()))
  def best_hand(hands) do
    hands
    |> Enum.map(&Hand.new/1)
    |> Enum.map(&SortedHand.new/1)
    |> select_winners
    |> Enum.map(& &1.raw)
  end

  def select_winners(hands) do
    highest_score = Enum.max_by(hands, & &1.score).score

    hands
    |> Enum.filter(&(&1.score == highest_score))
  end
end
