Code.load_file("card.exs", __DIR__)

defmodule Hand do
  defstruct [:cards, :raw]

  def new(input) do
    cards = Enum.map(input, &Card.new/1)

    %Hand{
      cards: cards,
      raw: input
    }
  end
end
