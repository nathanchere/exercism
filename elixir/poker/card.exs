defmodule Card do
  defstruct [:id, :rank, :suit, :value]

  def new(id) do
    {rank, suit} = String.split_at(id, -1)

    %Card{
      id: id,
      suit: to_suit(suit),
      rank: to_rank(rank),
      value: to_value(rank)
    }
  end

  def to_low(%Card{rank: :ace} = c) do
    {_, suit} = String.split_at(c.id, -1)
    new("1#{suit}")
  end

  defp to_suit("C"), do: :clubs
  defp to_suit("D"), do: :diamonds
  defp to_suit("H"), do: :hearts
  defp to_suit("S"), do: :spades

  defp to_value("A"), do: 14
  defp to_value("K"), do: 13
  defp to_value("Q"), do: 12
  defp to_value("J"), do: 11
  defp to_value(rank), do: String.to_integer(rank)

  defp to_rank("A"), do: :ace
  defp to_rank("K"), do: :king
  defp to_rank("Q"), do: :queen
  defp to_rank("J"), do: :jack
  defp to_rank(rank), do: rank
end
