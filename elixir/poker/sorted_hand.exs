Code.load_file("hand.exs", __DIR__)

use Bitwise

defmodule SortedHand do
  defstruct [:cards, :raw, :rank, :score]

  def new(%Hand{} = hand) do
    {rank, sorted_cards} = detect_rank(hand.cards)

    %SortedHand{
      raw: hand.raw,
      cards: sorted_cards,
      rank: rank,
      score: to_score_hash(rank, sorted_cards)
    }
  end

  def to_score_hash(
        hand_rank,
        [
          %Card{value: a} = _,
          %Card{value: b} = _,
          %Card{value: c} = _,
          %Card{value: d} = _,
          %Card{value: e} = _
        ]
      ) do
    handValue =
      case hand_rank do
        :straightflush -> 0x8
        :fourofakind -> 0x7
        :fullhouse -> 0x6
        :flush -> 0x5
        :straight -> 0x4
        :threeofakind -> 0x3
        :twopair -> 0x2
        :pair -> 0x1
        :highcard -> 0x0
      end

    e +
      (d <<< 4) +
      (c <<< 8) +
      (b <<< 12) +
      (a <<< 16) +
      (handValue <<< 20)
  end

  defp detect_rank(cards) do
    sorted_cards = Enum.sort_by(cards, &(-&1.value))
    sorted_low_straight = sorted_cards |> sort_low_straight
    grouped_cards = group_by_value(cards)

    cond do
      is_straight_flush?(sorted_cards) -> {:straightflush, sorted_cards}
      is_four_of_a_kind?(grouped_cards) -> {:fourofakind, grouped_cards}
      is_full_house?(grouped_cards) -> {:fullhouse, grouped_cards}
      is_flush?(sorted_cards) -> {:flush, sorted_cards}
      is_straight?(sorted_cards) -> {:straight, sorted_cards}
      is_straight?(sorted_low_straight) -> {:straight, sorted_low_straight}
      is_three_of_a_kind?(grouped_cards) -> {:threeofakind, grouped_cards}
      is_two_pair?(grouped_cards) -> {:twopair, grouped_cards}
      is_pair?(grouped_cards) -> {:pair, grouped_cards}
      true -> {:highcard, sorted_cards}
    end
  end

  defp group_by_value(cards) do
    cards
    |> Enum.group_by(& &1.value)
    |> Enum.map(fn {value, cards} -> {length(cards), value, cards} end)
    |> Enum.sort_by(fn {_, value, _} -> -value end)
    |> Enum.sort_by(fn {count, _, _} -> -count end)
    |> Enum.map(fn {_, _, card} -> card end)
    |> List.flatten()
  end

  defp sort_low_straight([%Card{value: 14} = a, b, c, d, e]), do: [b, c, d, e, Card.to_low(a)]
  defp sort_low_straight(cards), do: cards

  defp is_straight_flush?(cards), do: is_straight?(cards) && is_flush?(cards)

  defp is_four_of_a_kind?([
         %Card{value: quad} = _,
         %Card{value: quad} = _,
         %Card{value: quad} = _,
         %Card{value: quad} = _,
         %Card{value: _} = _
       ]),
       do: true

  defp is_four_of_a_kind?(_), do: false

  defp is_full_house?([
         %Card{value: triple} = _,
         %Card{value: triple} = _,
         %Card{value: triple} = _,
         %Card{value: pair} = _,
         %Card{value: pair} = _
       ]),
       do: true

  defp is_full_house?(_), do: false

  defp is_flush?([
         %Card{suit: suit} = _,
         %Card{suit: suit} = _,
         %Card{suit: suit} = _,
         %Card{suit: suit} = _,
         %Card{suit: suit} = _
       ]),
       do: true

  defp is_flush?(_), do: false

  defp is_straight?([
         %Card{value: a} = _,
         %Card{value: b} = _,
         %Card{value: c} = _,
         %Card{value: d} = _,
         %Card{value: e} = _
       ]) do
    a - 1 == b &&
      a - 2 == c &&
      a - 3 == d &&
      a - 4 == e
  end

  defp is_straight?(_), do: false

  defp is_three_of_a_kind?([
         %Card{value: triple} = _,
         %Card{value: triple} = _,
         %Card{value: triple} = _,
         _,
         _
       ]),
       do: true

  defp is_three_of_a_kind?(_), do: false

  defp is_two_pair?([
         %Card{value: high_pair} = _,
         %Card{value: high_pair} = _,
         %Card{value: low_pair} = _,
         %Card{value: low_pair} = _,
         %Card{value: _} = _
       ]),
       do: true

  defp is_two_pair?(_), do: false

  defp is_pair?([
         %Card{value: pair} = _,
         %Card{value: pair} = _,
         %Card{value: _} = _,
         %Card{value: _} = _,
         %Card{value: _} = _
       ]),
       do: true

  defp is_pair?(_), do: false
end
