defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
    |> to_charlist()
    |> Enum.map(fn c -> rotate_char(c, shift) end)
    |> List.to_string()
  end

  defp rotate_char(char, shift) when char >= ?a and char <= ?z do
    rotate_char_in_range(?a, ?z, char, shift)
  end

  defp rotate_char(char, shift) when char >= ?A and char <= ?Z do
    rotate_char_in_range(?A, ?Z, char, shift)
  end

  defp rotate_char(char, _), do: char

  defp rotate_char_in_range(min, max, value, shift) when value + shift > max do
    rotate_char_in_range(min, max, value - (max - min + 1), shift)
  end

  defp rotate_char_in_range(_, _, value, shift), do: value + shift
end
