defmodule Bob do
  @response_asked "Sure."
  @response_shouted "Whoa, chill out!"
  @response_shoutedquestion "Calm down, I know what I'm doing!"
  @response_silence "Fine. Be that way!"
  @response_default "Whatever."

  @doc """
  Returns a message based on the type of question asked.

  ## Examples

    iex> Bob.hey("")
      "Fine. Be that way!"
    iex> Bob.hey("Will you do this?")
      "Sure."

  """
  @spec hey(String.t()) :: String.t()
  def hey(input) do
    is_shouting = is_shouting?(input)
    is_asking = is_asking?(input)
    is_silence = is_silence?(input)

    cond do
      is_shouting and is_asking -> @response_shoutedquestion
      is_shouting -> @response_shouted
      is_asking -> @response_asked
      is_silence -> @response_silence
      true -> @response_default
    end
  end

  defp is_silence?(input) do
    String.trim(input)
    |> String.length() == 0
  end

  defp is_shouting?(input) do
    upcase = String.upcase(input)
    upcase == input && upcase != String.downcase(input)
  end

  defp is_asking?(input), do: input |> String.ends_with?("?")
end
