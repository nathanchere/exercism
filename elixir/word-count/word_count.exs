defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t()) :: map
  def count(sentence) do
    sentence
    |> clean
    |> String.split(" ", trim: true)
    |> Enum.reduce(%{}, &count_words/2)
  end

  defp clean(input) do
    Regex.replace(~r/[^[:alnum:]\-]/u, input, " ")
    |> String.downcase()
  end

  defp count_words(word, result) do
    result
    |> Map.update(word, 1, &(&1 + 1))
  end
end
