defmodule BeerSong do
  @doc """
  Get a single verse of the beer song
  """
  @spec verse(integer) :: String.t()
  def verse(number) do
    a = x_bottles_of_beer(number)
    a_cap = a |> String.capitalize()
    b = take_one_down(number)
    c = ending_bottles(number)

    """
    #{a_cap} on the wall, #{a}.
    #{b}, #{c} on the wall.
    """
  end

  @doc """
  Get the entire beer song for a given range of numbers of bottles.
  """
  @spec lyrics(Range.t()) :: String.t()
  def lyrics(range \\ 99..0) do
    Enum.map_join(range, "\n", &verse/1)
  end

  defp x_bottles_of_beer(0), do: "no more bottles of beer"
  defp x_bottles_of_beer(1), do: "1 bottle of beer"
  defp x_bottles_of_beer(number), do: "#{number} bottles of beer"

  defp take_one_down(0), do: "Go to the store and buy some more"
  defp take_one_down(1), do: "Take it down and pass it around"
  defp take_one_down(n), do: "Take one down and pass it around"

  defp ending_bottles(0), do: x_bottles_of_beer(99)
  defp ending_bottles(n), do: x_bottles_of_beer(n - 1)
end
