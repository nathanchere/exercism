defmodule SecretHandshake do
  @doc """
  Determine the actions of a secret handshake based on the binary
  representation of the given `code`.

  If the following bits are set, include the corresponding action in your list
  of commands, in order from lowest to highest.

  1 = wink
  10 = double blink
  100 = close your eyes
  1000 = jump

  10000 = Reverse the order of the operations in the secret handshake
  """

  @spec commands(code :: integer) :: list(String.t())
  def commands(code) do
    input =
      code
      |> decimal_to_binary
      |> first_x_significant_digits(5)
      |> decode_handshake()
  end

  defp decimal_to_binary(input) do
    input
    |> Integer.to_string(2)
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
  end

  defp first_x_significant_digits(input, x) do
    (List.duplicate(0, x) ++ input)
    |> Enum.take(-x)
  end

  defp decode_handshake([a, b, c, d, e]), do: decode_handshake({[a, b, c, d, e], []})

  defp decode_handshake({[a, b, c, d, 1], result}) do
    decode_handshake({[a, b, c, d, 0], result ++ ["wink"]})
  end

  defp decode_handshake({[a, b, c, 1, 0], result}),
    do: decode_handshake({[a, b, c, 0, 0], result ++ ["double blink"]})

  defp decode_handshake({[a, b, 1, 0, 0], result}),
    do: decode_handshake({[a, b, 0, 0, 0], result ++ ["close your eyes"]})

  defp decode_handshake({[a, 1, 0, 0, 0], result}),
    do: decode_handshake({[a, 0, 0, 0, 0], result ++ ["jump"]})

  defp decode_handshake({[1, 0, 0, 0, 0], result}),
    do: Enum.reverse(result)

  defp decode_handshake({_, result}), do: result
end
