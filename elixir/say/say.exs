defmodule Say do
  @max_number 1_000_000_000_000
  @error_oor {:error, "number is out of range"}

  @unit_names %{
    0 => "",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety"
  }

  @blocks [
    {1_000_000_000_000_000_000_000_000, "septillion"},
    {1_000_000_000_000_000_000_000, "sextillion"},
    {1_000_000_000_000_000_000, "quintillion"},
    {1_000_000_000_000_000, "quadrillion"},
    {1_000_000_000_000, "trillion"},
    {1_000_000_000, "billion"},
    {1_000_000, "million"},
    {1_000, "thousand"},
    {1, ""}
  ]

  @doc """
  Translate a positive integer into English.
  """
  @spec in_english(integer) :: {atom, String.t()}
  def in_english(number) when number < 0, do: @error_oor
  def in_english(number) when number >= @max_number, do: @error_oor
  def in_english(0), do: {:ok, "zero"}

  def in_english(number) do
    {_, fragments} =
      Enum.reduce(@blocks, {number, []}, fn block, acc -> parse_block(block, acc) end)

    result =
      fragments
      |> Enum.reverse()
      |> Enum.join(" ")
      |> String.trim()

    {:ok, result}
  end

  def parse_block({unit, _}, {number, result}) when number < unit, do: {number, result}

  def parse_block({unit, unit_name}, {number, result}) do
    block = div(number, unit)
    remainder = rem(number, unit)

    case block do
      0 ->
        {remainder, result}

      _ ->
        block_text = parse_hundreds(block)
        {remainder, ["#{block_text} " <> unit_name] ++ result}
    end
  end

  def parse_hundreds(number) when number >= 100 and number <= 999 do
    tens = rem(number, 100)
    hundreds_text = @unit_names[div(number, 100)] <> " hundred"

    case tens do
      0 ->
        hundreds_text

      _ ->
        tens_text = parse_tens(tens)
        "#{hundreds_text} #{tens_text}"
    end
  end

  def parse_hundreds(number) when number <= 99, do: parse_tens(number)

  def parse_tens(number) when number >= 21 and number <= 99 do
    tens = div(number, 10) * 10
    ones = rem(number, 10)

    case ones do
      0 -> "#{@unit_names[tens]}"
      _ -> "#{@unit_names[tens]}-#{@unit_names[ones]}"
    end
  end

  def parse_tens(number) when number <= 20 do
    @unit_names[number]
  end
end
