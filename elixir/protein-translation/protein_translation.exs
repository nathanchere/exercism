defmodule ProteinTranslation do
  @stop "STOP"

  @spec of_rna(String.t()) :: {atom, list(String.t())}
  def of_rna(rna), do: parse(rna, [])

  defp parse(<<>>, result), do: {:ok, result}

  defp parse(<<codon::binary-3, tail::binary>>, result) do
    case of_codon(codon) do
      {:ok, @stop} -> {:ok, result}
      {:ok, codon} -> parse(tail, result ++ [codon])
      _ -> {:error, "invalid RNA"}
    end
  end

  @spec of_codon(String.t()) :: {atom, String.t()}
  def of_codon(codon) when codon in ["UGU", "UGC"], do: {:ok, "Cysteine"}
  def of_codon(codon) when codon in ["UUA", "UUG"], do: {:ok, "Leucine"}
  def of_codon("AUG"), do: {:ok, "Methionine"}
  def of_codon(codon) when codon in ["UUU", "UUC"], do: {:ok, "Phenylalanine"}
  def of_codon(codon) when codon in ["UCU", "UCC", "UCA", "UCG"], do: {:ok, "Serine"}
  def of_codon("UGG"), do: {:ok, "Tryptophan"}
  def of_codon(codon) when codon in ["UAU", "UAC"], do: {:ok, "Tyrosine"}
  def of_codon(codon) when codon in ["UAA", "UAG", "UGA"], do: {:ok, @stop}
  def of_codon(_), do: {:error, "invalid codon"}
end
