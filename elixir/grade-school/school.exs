defmodule School do
  @moduledoc """
  Simulate students in a school.

  Each student is in a grade.
  """

  @doc """
  Add a student to a particular grade in school.
  """
  @spec add(map, String.t(), integer) :: map
  def add(db, name, grade) do
    students = Map.get(db, grade, [])
    Map.put(db, grade, [name] ++ students)
  end

  @doc """
  Return the names of the students in a particular grade.
  """
  @spec grade(map, integer) :: [String.t()]
  def grade(db, grade),
    do:
      Map.get(db, grade, [])
      |> Enum.sort()

  @doc """
  Sorts the school by gradz2e and name.
  """
  @spec sort(map) :: [{integer, [String.t()]}]
  def sort(db) do
    Map.keys(db)
    |> Enum.sort()
    |> Enum.map(fn g -> {g, grade(db, g)} end)
  end
end
