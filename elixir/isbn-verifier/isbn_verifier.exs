defmodule ISBNVerifier do
  @doc """
    Checks if a string is a valid ISBN-10 identifier

    ## Examples

      iex> ISBNVerifier.isbn?("3-598-21507-X")
      true

      iex> ISBNVerifier.isbn?("3-598-2K507-0")
      false

  """
  @spec isbn?(String.t()) :: boolean
  def isbn?(isbn) do
    case is_valid_format?(isbn) do
      true ->
        isbn
        |> parse
        |> is_valid?

      false ->
        false
    end
  end

  defp is_valid_format?(isbn), do: Regex.match?(~r/^\d\-?\d{3}\-?\d{5}\-?[\dX]$/, isbn)

  defp parse(isbn) do
    String.replace(isbn, "-", "")
    |> String.graphemes()
    |> Enum.map(&parse_digit/1)
  end

  defp parse_digit("X"), do: 10
  defp parse_digit(digit), do: String.to_integer(digit)

  defp is_valid?(digits) do
    Enum.zip(digits, 10..1)
    |> Enum.reduce(0, fn {x, y}, acc -> acc + x * y end)
    |> rem(11) == 0
  end
end
