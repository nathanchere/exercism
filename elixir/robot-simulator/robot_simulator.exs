defmodule RobotSimulator do
  @type position :: {integer(), integer()}
  @compass_directions [:north, :west, :south, :east]

  def create(direction \\ :north, position \\ {0, 0})

  def create(direction, {x, y})
      when direction in @compass_directions and
             is_number(x) and is_number(y) do
    Robot.new(direction, {x, y})
  end

  def create(_, {x, y}) when is_number(x) and is_number(y), do: {:error, "invalid direction"}
  def create(_, _), do: {:error, "invalid position"}

  def simulate(robot, instructions) do
    instructions
    |> String.graphemes()
    |> Enum.reduce(robot, &Robot.move/2)
  end

  def direction(robot), do: robot.direction
  def position(robot), do: robot.position
end

defmodule Robot do
  defstruct [:position, :direction]

  @compass_directions [:north, :west, :south, :east]

  def new(direction, position) do
    %Robot{
      position: position,
      direction: direction
    }
  end

  def move(command, %Robot{} = robot) when command in ["L", "R"],
    do: %Robot{
      position: robot.position,
      direction: rotate(robot.direction, command)
    }

  def move("A", %Robot{} = robot),
    do: %Robot{
      position: translate_positon(robot.position, to_offset(robot.direction)),
      direction: robot.direction
    }

  def move(_, _), do: {:error, "invalid instruction"}

  defp translate_positon({x, y}, {i, j}), do: {x + i, y + j}

  defp to_offset(direction) do
    case(direction) do
      :north -> {0, 1}
      :west -> {-1, 0}
      :south -> {0, -1}
      :east -> {1, 0}
    end
  end

  defp rotate(:north, "L"), do: :west
  defp rotate(:north, "R"), do: :east
  defp rotate(:west, "L"), do: :south
  defp rotate(:west, "R"), do: :north
  defp rotate(:south, "L"), do: :east
  defp rotate(:south, "R"), do: :west
  defp rotate(:east, "L"), do: :north
  defp rotate(:east, "R"), do: :south
end
