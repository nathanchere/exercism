defmodule BinarySearch do
  @doc """
    Searches for a key in the tuple using the binary search algorithm.
    It returns :not_found if the key is not in the tuple.
    Otherwise returns {:ok, index}.

    ## Examples

      iex> BinarySearch.search({}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 2)
      :not_found

      iex> BinarySearch.search({1, 3, 5}, 5)
      {:ok, 2}

  """

  @spec search(tuple, integer) :: {:ok, integer} | :not_found
  def search({}, _), do: :not_found

  def search(numbers, key) do
    high = tuple_size(numbers) - 1
    do_search(numbers, key, 0, mid(0, high), high)
  end

  defp do_search(numbers, key, low, _, high) when high - low <= 1 do
    cond do
      elem(numbers, low) == key -> {:ok, low}
      elem(numbers, high) == key -> {:ok, high}
      true -> :not_found
    end
  end

  defp do_search(numbers, key, low, middle, high) do
    current_value = elem(numbers, middle)

    {next_low, next_mid, next_high} =
      cond do
        current_value == key ->
          {middle, middle, middle}

        current_value < key ->
          {middle, mid(middle, high), high}

        current_value > key ->
          {low, mid(low, middle), middle}
      end

    do_search(numbers, key, next_low, next_mid, next_high)
  end

  defp mid(low, high), do: low + floor((high - low) * 0.5)
end
