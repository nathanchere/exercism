defmodule Scrabble do
  @doc """
  Calculate the scrabble score for the word.
  """
  @spec score(String.t()) :: non_neg_integer
  def score(word) do
    word
    |> String.upcase()
    |> String.replace(~r/[^A-Z]/s, "")
    |> String.graphemes()
    |> Enum.map(&tile_to_points/1)
    |> Enum.sum()
  end

  defp tile_to_points(tile) do
    cond do
      String.contains?("AEIOULNRST", tile) -> 1
      String.contains?("DG", tile) -> 2
      String.contains?("BCMP", tile) -> 3
      String.contains?("FHVWY", tile) -> 4
      "K" == tile -> 5
      String.contains?("JX", tile) -> 8
      String.contains?("QZ", tile) -> 10
      true -> 0
    end
  end
end
